xquery version "3.1" encoding "utf-8"; (: BaseX 9.3 :)

(: written by Giuseppe G. A. Celano :)

declare namespace xlink="http://www.w3.org/1999/xlink";
declare namespace tei="http://www.tei-c.org/ns/1.0";
declare option db:chop 'false';
declare variable $from external;
declare variable $type external;

(: the following recursive function serves to merge together
   those tokens which are separated as in, e.g.,
   te<add>m</add>tatas in phi0836.phi002.perseus-lat6 :)
declare function local:recursive($pre_marks, $marks)
  {
    let $mark   := head($marks)
    let $href2  := $pre_marks[last()]
    let $intsum := $href2/@start + $href2/@long
    let $pre_marks := if
                       (count($pre_marks) = 0)
                       then $mark

                       else if
                       ($href2/@href = $mark/@href and
                        $intsum  = $mark/@start
                        and not($mark[@m = "cabbr"])
                                                (: the func is applied
                                                         after cabbr are
                                                         separated
                                                :)

                        and not($mark[@m = "punct"])
                        and not($mark[@m = "foreign"]))
                       then
                       (
                       let $y := count($pre_marks)
                       let $c := $pre_marks[position() < $y]
                       return
                       (
                       $c,
                       element mark {attribute href {$href2/@href},
                                     attribute start {$href2/@start},
                                     attribute long {$href2/@long + $mark/@long
                                     }
                       ,
                        $mark/(@* except (@href, @start, @long)),
                        $href2/text() || $mark/text()
                       })
                       )
                       else
                       ($pre_marks,$mark)
      return (
        if(count($marks) > 1) then
          local:recursive($pre_marks, tail($marks))
        else $pre_marks
      )
  };

declare variable $abb :=
 fetch:xml(
  "https://git.informatik.uni-leipzig.de/celano/latinnlp/raw/master/" ||
  "abbreviations/abbr-in-texts.xml"
         );

(: the xml file is converted into maps for performance reasons :)
declare variable $abbS :=
 for $i in $abb//a
 return
  map {"f" : data($i/@f)}?f ;

declare variable $to-tokenize :=
 fetch:xml(
  "https://git.informatik.uni-leipzig.de/celano/latinnlp/raw/master/" ||
  "tokenize/to-tokenize.xml"
          );

(: the xml file is converted into maps for performance reasons :)
declare variable $to-tokenizeS :=
 for $o in $to-tokenize//w
 let $t1 := $o/t[1]
 let $t2 := $o/t[2]
 let $t3 := $o/t[3]
 return
  map
   {"f" : $o/@f/data(),
    "t1" : map {"f" : data($t1/text()), "s" : data($t1/@s), "l": data($t1/@l)},
    "t2" : map {"f" :  data($t2/text()), "s" : data($t2/@s), "l": data($t2/@l)},
    "t3" :
      if ($t3)
      then map {"f" :  data($t3/text()), "s" : data($t3/@s), "l": data($t3/@l)}
      else ()
   };

declare variable $to-tokenizeS1 :=
 $to-tokenizeS?f;

declare variable $not-to-tokenize :=
 fetch:xml(
  "https://git.informatik.uni-leipzig.de/celano/latinnlp/raw/master/" ||
  "tokenize/not-to-tokenize.xml"
          );

(: the xml file is converted into maps for performance reasons :)
declare variable $not-to-tokenizeS :=
 for $i in $not-to-tokenize//w
 return
  map {"f" : data($i/@f), "pn" : data($i/@pn)};

declare variable $not-to-tokenizeS1 :=
 $not-to-tokenizeS?f;

declare  function local:ws-tok($a)
{
(: the following let clause identifies the CTS structure which is already
   specified in each TEI/EPIDOC file: this means that any part of the XML file
   which is not the main text of a work is filtered out  :)

 let $cRefPattern := $a/tei:TEI/tei:teiHeader/tei:encodingDesc
                    /tei:refsDecl[@n="CTS"]/tei:cRefPattern
 let $l := count($cRefPattern) (: this is useful for creating token ids :)
 let $p := $cRefPattern[1]/@replacementPattern
          => replace("(#xpath\()(.*)(\))", "$2")
          => replace("tei", "*")
          => replace("\[.*?\]", "")

 for $text in xquery:eval($p, map { '': $a  })
 let $path := path($text)
 return
  let $e:=  element {"div"}
                    {attribute {"path"} {$path},
                     for $u in $text//text()
                     return
                     element {$u/parent::*/name()}
                     {attribute anc {$u/ancestor::*/name()},
                      fn:analyze-string($u, "\s")}
                    }

(: the following for clause is the point where the content of some elements
   can be filtered out :)

(: some elements, such as, for example, "listPerson"
   are automatically filtered because
   they do not fall within the scope of the xpath defined
   by the tei:cRefPattern, while others are because
   they are present in already filtered-out elements
   (e.g., "lem" always in "app").
:)

  for $tok at $c in $e//fn:non-match
               [not(matches(ancestor::*/@anc, "note"))]
               [not(matches(ancestor::*/@anc, "app"))]
               [not(matches(ancestor::*/@anc, "bibl"))]
               [not(matches(ancestor::*/@anc, "del"))]
               [not(matches(ancestor::*/@anc, "desc"))]
               [not(matches(ancestor::*/@anc, "docAuthor"))]
               [not(matches(ancestor::*/@anc, "figure"))]
               [not(matches(ancestor::*/@anc, "ref"))]
               [not(matches(ancestor::*/@anc, "speaker"))]
               [not(matches(ancestor::*/@anc, "stage"))]


  let $pos := sum($tok/(preceding::fn:match union preceding::fn:non-match)
                                                              /string-length(.))
  let $length := string-length($tok)
  return

  if       ($tok/ancestor::*:sic[ends-with(@anc, "choice sic")])
  then
   <mark href="{replace($e/@path, "Q\{http://www.tei-c.org/ns/1.0\}", "*:")}"
       start="{$pos + 1}" long="{$length}" lev="{$l}" m="csic"
       anc="{$tok/ancestor::*/@anc}">{$tok/text()}</mark>

  else if  ($tok/ancestor::*:corr[ends-with(@anc, "choice corr")])
  then
  <mark href="{replace($e/@path, "Q\{http://www.tei-c.org/ns/1.0\}", "*:")}"
       start="{$pos + 1}" long="{$length}" lev="{$l}" m="ccorr"
       anc="{$tok/ancestor::*/@anc}">{$tok/text()}</mark>

  else if ($tok/ancestor::*:abbr[ends-with(@anc, "choice abbr")])
  then
  <mark href="{replace($e/@path, "Q\{http://www.tei-c.org/ns/1.0\}", "*:")}"
       start="{$pos + 1}" long="{$length}" lev="{$l}" m="cabbr"
       anc="{$tok/ancestor::*/@anc}">{$tok/text()}</mark>

  else if ($tok/ancestor::*:expan[ends-with(@anc, "choice expan")])
  then
  <mark href="{replace($e/@path, "Q\{http://www.tei-c.org/ns/1.0\}", "*:")}"
       start="{$pos + 1}" long="{$length}" lev="{$l}" m="cexpan"
       anc="{$tok/ancestor::*/@anc}">{$tok/text()}</mark>

  else if ($tok/ancestor::*:reg[ends-with(@anc, "choice reg")])
  then
  <mark href="{replace($e/@path, "Q\{http://www.tei-c.org/ns/1.0\}", "*:")}"
       start="{$pos + 1}" long="{$length}" lev="{$l}" m="creg"
       anc="{$tok/ancestor::*/@anc}">{$tok/text()}</mark>

  else if ($tok/ancestor::*:orig[ends-with(@anc, "choice orig")])
  then
  <mark href="{replace($e/@path, "Q\{http://www.tei-c.org/ns/1.0\}", "*:")}"
       start="{$pos + 1}" long="{$length}" lev="{$l}" m="corig"
       anc="{$tok/ancestor::*/@anc}">{$tok/text()}</mark>

  else if ($tok/ancestor::*:foreign[ends-with(@anc, "foreign")])
  then
  <mark href="{replace($e/@path, "Q\{http://www.tei-c.org/ns/1.0\}", "*:")}"
       start="{$pos + 1}" long="{$length}" lev="{$l}" m="foreign"
       anc="{$tok/ancestor::*/@anc}">{$tok/text()}</mark>

  else if ($tok/ancestor::*:add[ends-with(@anc, "add")])
  then
  <mark href="{replace($e/@path, "Q\{http://www.tei-c.org/ns/1.0\}", "*:")}"
       start="{$pos + 1}" long="{$length}" lev="{$l}" m="add"
       anc="{$tok/ancestor::*/@anc}">{$tok/text()}</mark>

  else if (string-length($tok) < 3 and matches($tok, "\p{P}"))
  then
  <mark href="{replace($e/@path, "Q\{http://www.tei-c.org/ns/1.0\}", "*:")}"
       start="{$pos + 1}" long="{$length}" lev="{$l}" m="punct"
       anc="{$tok/ancestor::*/@anc}">{$tok/text()}</mark>

  else
   <mark href="{replace($e/@path, "Q\{http://www.tei-c.org/ns/1.0\}", "*:")}"
       start="{$pos + 1}" long="{$length}" lev="{$l}"
       anc="{$tok/ancestor::*/@anc}">{$tok/text()}</mark>

};

declare function local:manage-tag($a)
{
 for $m at $c in $a
 return
  if ($m[@m ="csic"]) then ()

  else if ($m[@m ="cexpan"]) then ()

  (: I assume the case percontatu's :)
  else if ($m[@m ="cabbr"] and matches($m, "\p{L}'\p{L}"))
  then

  let $j := tokenize($m, "'")
  return
  (
  element mark {$m/@href, $m/@start, attribute long {fn:string-length($j[1])},
                $m/@lev, $m/@m, $m/@anc, $j[1]},
  element mark {$m/@href, attribute start {$m/@start + fn:string-length($j[1])},
                attribute long {1+fn:string-length($j[2])},
                $m/@lev, $m/@m, $m/@anc, "'" || $j[2] }
  )

  else if ($m[@m ="corig"]) then ()

  else $m
};

declare function local:merge-add($a)
{
  local:recursive((), $a)
};

declare function local:create-paula-primary-text-text($a, $filename){
 <paula version="1.1">
	<header paula_id="{$filename}" type="TEXT"></header>
  <body>{file:read-text($a)}</body>
 </paula>
};

(: the following function is to calcultate offsets if the original file is txt:)
declare function local:calculate-offset($position, $marks)
 {
     let $mark   := head($marks)
     let $position := if(count($position) = 0)
      then (<mark start="1" long="{string-length($mark)}">{$mark/text()}</mark>)
      else (
          $position,
          element mark { attribute start {$position[count($position)]/@start
                                         + $position[count($position)]/@long},
                         attribute long {string-length($mark)}, $mark/text()}
             )
     return (
        if(count($marks) > 1) then
          local:calculate-offset($position, tail($marks))
        else $position
      )
};

(: this outputs a txt text tokenized without whitespaces, with offsets :)
declare function local:text($a) {
for $u in $a//body
return
local:calculate-offset((), analyze-string($u, "\s+")//*)
[not(matches(., "\s+"))]
};

declare function local:avoid-abbr($a)
{
 for $t in $a
 let $s := $t[matches(., "^\p{Lu}{1}\.$")]
 let $b := $t[. = $abbS]
 let $d := $t[@m="cabbr"]
 return
  if ($s or $b or $d) then $t else
   for $tok in fn:analyze-string($t, "\p{P}")/*
   let $pos := sum($tok/preceding-sibling::*/text()/string-length(.))
   let $length := string-length($tok)
   return
    <mark href="{$t/@href}"
          start="{$t/@start + $pos}" long="{$length}"
          lev="{$t/@lev}">{$t/@m,
          $tok/text()}</mark>
};

declare  function local:to-tok($a)
{
 for $a in $a
 let $p := $to-tokenizeS
 let $b := $a[. = $to-tokenizeS1]

 return
  if ($b) then
   for $h in $p[?f = $a]?('t1', 't2', 't3')
   return
    <mark href="{$a/@href}" start="{$a/@start + $h?s -1}"
          long="{$h?l}" lev="{$a/@lev}">{$a/@m,  $h?f}</mark>
  else $a
};

declare function local:que-ve-ne($a)
{
 for $j in $a
 let $o := replace($j, "j", "i")
           => replace("J", "I")
           => normalize-unicode('NFD')   (: to avoid accented words in poetry:)
           => replace('\p{IsCombiningDiacriticalMarks}', '')
 let $a := $o[. = $not-to-tokenizeS1]
 let $q := matches($o, ".*.cumque$") or matches($o, ".*.cúmque$")
 let $n := matches($o, ".*.cunque$")
 let $v := matches($o, ".*.quomque$")
 let $h := matches($o, "one$|ine$|óne$|íne$")
 let $e := fn:analyze-string($j, "que$|ve$|ue$|ne$")
 return
  if (not($e/fn:match) or $a or $q or $n or $v or $h)
   then $j else
    for $tok in $e/*
    let $pos := sum($tok/preceding-sibling::*/text()/string-length(.))
    let $length := string-length($tok)
    return
     <mark href="{$j/@href}"
            start="{$j/@start + $pos}" long="{$length}"
            lev="{$j/@lev}">{
              $j/@m, $tok/text()}</mark>
};

declare function local:transform-mark($a)
{
 for $m in $a
 let $g := $m/@href
 group by $g
 return
  for $t in $m
  return
   (<mark
     xlink:href=
      "{'#xpointer(string-range(' || $t/@href || ',&apos;&apos;,' ||
      $t/@start || ',' || $t/@long || '))'}"
     com ="{data($t)}"
    >{$t/@m}</mark>
   )
};

(: if element names such as "add" or "sic" need to be printed add
   $m/@m :)

declare function local:finalize-xml($a)
{
  for $m in $a
  count $c
  return
  element mark {attribute id {"tok_" || $c}, $m/@*:href, $m/@com} (: <= $m/@m :)
};


declare function local:finalize-xml-print($a)
{
  for $m in $a
  return
  (element mark {$m/@id, $m/@*:href}, (: <= $m/@m :)
  if (matches($m/@com, "-")) then
  <!--&#45;--> else comment {data($m/@com)})
};

declare function local:finalize-text($a)
{
  for $m in $a
  count $c
  return
  element mark {attribute id {"tok_" || $c}, $m/@start, $m/@long, $m/text()}
};

declare function local:finalize-text-print($a)
{
  for $m in $a
  return
  (element mark {$m/@id,
           attribute xlink:href {"#xpointer(string-range(//body,''," ||
                                 $m/@start || "," || $m/@long || "))"}
                                },
  if (matches($m/text(), "-")) then
  <!--&#45;--> else comment {data($m/text())})
};

(:
   this function is used as an input to identify sentences, because
   texts in comments (see following function) requires more time to be retrieved
:)
declare function  local:create-paula-mark-xml($path_file, $name_file, $name_base)
{

 <paula version="1.1">
 <header paula_id="{$name_file}"/>
 <markList xmlns:xlink="http://www.w3.org/1999/xlink" type="tok"
          xml:base="{$name_base}">
{
 doc($path_file) => local:ws-tok() => local:manage-tag() => local:merge-add() =>
 local:avoid-abbr() => local:to-tok() => local:que-ve-ne()
 => local:transform-mark() => local:finalize-xml()
}
 </markList>
 </paula>
};

declare function  local:create-paula-mark-xml-print($a)
{

 <paula version="1.1">
 <header paula_id="{$a/header/@paula_id}"/>
 <markList xmlns:xlink="http://www.w3.org/1999/xlink" type="tok"
          xml:base="{$a/markList/@xml:base}">
{
 local:finalize-xml-print($a//mark)
}
 </markList>
 </paula>
};


declare function local:create-paula-mark-text($path_file, $name_file,
                                                                     $name_base)
{

 <paula version="1.1">
 <header id="{$name_file}"/>
 <markList xmlns:xlink="http://www.w3.org/1999/xlink" type="tok"
           xml:base="{$name_base}">
{
 local:text($path_file) =>
 local:avoid-abbr() => local:to-tok() => local:que-ve-ne()
  => local:finalize-text()
}
 </markList>
 </paula>
};

declare function local:create-paula-mark-text-print($a)
{

 <paula version="1.1">
 <header id="{$a/header/@id}"/>
 <markList xmlns:xlink="http://www.w3.org/1999/xlink" type="tok"
           xml:base="{$a/markList/@xml:base}">
{
 local:finalize-text-print($a//mark)
}
 </markList>
 </paula>
};

(: start sentence-split related functions :)

declare function local:create-sentence($tt2)
{

for tumbling window $s in $tt2//mark
start  $a when fn:true()

only end $b previous $v next $r when


(: start the following criteria aims to capture cases such as, e.g. ]. or .] :)

(
$b/matches(@com, "[\p{Pe}\p{Pf}&#39;&#34;]")
and
$v/matches(@com, "^[\.?!:;]") (: ^ to skip C. :)
)
or
(
$b/matches(@com, "^[\.?!:;]") (: ^ to skip C. :)
and
not(
$r/matches(@com, "[\p{Pe}\p{Pf}&#39;&#34;]")
   )
)

count $r2
return
(<mark id="{"clause_" || $r2}"
      xlink:href="{
                 "(" ||
                 string-join(
                 for $m in $s/@id/data()
                 return
                 "#" || $m, ",")
                 || ")"
                 }"
/>,


(: the following comment reconstructs the  sentence just putting together the
   tokens, each of them being separated, by default, by a space :)
(: parse-xml-fragment converts -&#45; into - :)
   comment{

    $s/@com

   },

(: the following comment reconstructs the graphic sentence with the following
   logic: if the XPath expression of a token and that of the immediately
   following one is the same AND if the latter starts immediately after the
   former then the tokens are univerbated (i.e., this happens with punctuation
   marks). In all the other cases I add a space between the tokens by default:
   i.e. I assume that if the XPath expressions are different or there is some
   character between two tokens than this should be a space, since anything else
   between them does not belong to the sentence syntactically analyzed. This
   therefore means that in the original text there might be word(s) between
   two tokens :)

    comment {


    string-join(

    for $i at $c in $s
    let $f := fn:analyze-string($i/@xlink:href,
              "(#xpointer\(string-range\()(.*)(,'',)(.*)(,)(.*)(\)\))")
    let $i2 := $s[position() = $c + 1]
    let $f2 := fn:analyze-string($i2/@xlink:href,
              "(#xpointer\(string-range\()(.*)(,'',)(.*)(,)(.*)(\)\))")
    return

    (: the following if is for the last token which ends the sentence without
       anything else following :)
    if ($i/@id = $s[last()]/@id) then $i/@com  => replace(" ", "")

    else
    if ($f//fn:group[@nr="2"] = $f2//fn:group[@nr="2"]
        and
        $f//fn:group[@nr="4"] + $f//fn:group[@nr="6"] = $f2//fn:group[@nr="4"]
       )
    then  $i/@com
    else  $i/@com || " "

    , "")

      }

  )
};

declare function local:create-sentence-text($tt2)
{

for tumbling window $s in $tt2//mark
start  $a when fn:true()

only end $b previous $v next $r when


(: start the following criteria aims to capture cases such as, e.g. ]. or .] :)

(
$b/matches(., "[\p{Pe}\p{Pf}&#39;&#34;]")
and
$v/matches(., "^[\.?!:;]") (: ^ to skip C. :)
)
or
(
$b/matches(., "^[\.?!:;]") (: ^ to skip C. :)
and
not(
$r/matches(., "[\p{Pe}\p{Pf}&#39;&#34;]")
   )
)

count $r2
return
(<mark id="{"clause_" || $r2}"
      xlink:href="{
                 "(" ||
                 string-join(
                 for $m in $s/@id/data()
                 return
                 "#" || $m, ",")
                 || ")"
                 }"
/>,


(: the following comment reconstructs the  sentence just putting together the
   tokens, each of them being separated, by default, by a space :)
(: parse-xml-fragment converts -&#45; into - :)
   comment{

    $s/text()

   },
   comment{

    string-join(
    for $m at $c in $s
    return
    if ($m/@start + $m/@long = $s[$c + 1]/@start or $c = $s/last())
    then $m/text()
    else $m/text() || " "
    , "")

   }
  )
};

declare function local:create-sentence-text-conllu($tt2)
{

for tumbling window $s in $tt2//mark
start  $a when fn:true()

only end $b previous $v next $r when


(: start the following criteria aims to capture cases such as, e.g. ]. or .] :)

(
$b/matches(., "[\p{Pe}\p{Pf}&#39;&#34;]")
and
$v/matches(., "^[\.?!:;]") (: ^ to skip C. :)
)
or
(
$b/matches(., "^[\.?!:;]") (: ^ to skip C. :)
and
not(
$r/matches(., "[\p{Pe}\p{Pf}&#39;&#34;]")
   )
)

count $r2
let $text := string-join(for $m at $c0 in $s
                         return
                          if ($m/@start + $m/@long = $s[$c0 + 1]/@start
                                                            or $c0 = $s/last())
                          then $m/text()
                          else $m/text() || " "
                     , "")
return
 "# sent_id = " || $r2 || "&#xA;" ||
 "# text = " || $text || "&#xA;" ||
 string-join(
   for $m at $c in $s
   let $cond := if ($m/@start + $m/@long = $s[$c + 1]/@start)
                 then "SpaceAfter=No"
                 else "_"
   return
   $c || "&#x9;" || $m/text() || "	_	_	_	_	_	_	_	" || $cond
   , "&#xA;")
   || "&#xA;"
};

declare function local:create-sentence-text-conllu2($tt2)
{

for tumbling window $s in $tt2//mark
start  $a when fn:true()

only end $b previous $v next $r when


(: start the following criteria aims to capture cases such as, e.g. ]. or .] :)

(
$b/matches(., "[\p{Pe}\p{Pf}&#39;&#34;]")
and
$v/matches(., "^[\.?!:;]") (: ^ to skip C. :)
)
or
(
$b/matches(., "^[\.?!:;]") (: ^ to skip C. :)
and
not(
$r/matches(., "[\p{Pe}\p{Pf}&#39;&#34;]")
   )
)

count $r2
let $text := string-join(for $m at $c0 in $s
                         return
                          if ($m/@start + $m/@long = $s[$c0 + 1]/@start)
                          then $m/text()
                          else $m/text() || " "
                     , "")
return
 "# sent_id = " || $r2 || "&#xA;" ||
 "# text = " || $text || "&#xA;" ||
 string-join(
   for $m at $c in $s
   return
   if ($m/text() || $s[$c + 1]/text() || $s[$c + 2]/text()
      = $to-tokenizeS?f)
   then

   string-join(($c || "-" || $c + 2 || "&#x9;" || $m/text() || $s[$c + 1]/text()
    || $s[$c + 2]/text()  || "	_	_	_	_	_	_	_	" || "SpaceAfter=No",
   $c || "&#x9;" || $m/text() || "	_	_	_	_	_	_	_	" || "_",
   $c + 1 || "&#x9;" || $s[$c + 1]/text() || "	_	_	_	_	_	_	_	" || "_",
   $c + 2 || "&#x9;" || $s[$c + 2]/text() || "	_	_	_	_	_	_	_	" || "_"
   )
              , "&#xA;")

   else if ($s[$c - 1] || $m || $s[$c + 1]
                    = $to-tokenizeS?f) then ()

   else if ($s[$c - 2] || $s[$c - 1] || $m
                    = $to-tokenizeS?f) then ()

   else if ($m/@start + $m/@long = $s[$c + 1]/@start
            and
            not(matches($m, "[\p{P}\p{Pe}\p{Pf}]"))
            and
            not(matches($s[$c + 1], "[\p{P}\p{Pe}\p{Pf}]"))
           )
   then

   string-join(($c || "-" || $c + 1 || "&#x9;" || $m/text() || $s[$c + 1]/text()
             || "	_	_	_	_	_	_	_	" || "SpaceAfter=No",
      $c || "&#x9;" || $m/text() || "	_	_	_	_	_	_	_	" || "_"), "&#xA;")

   else
    $c || "&#x9;" || $m/text() || "	_	_	_	_	_	_	_	" || "_"
   , "&#xA;")

   || "&#xA;"

};

declare function local:create-paula-mark-sent-text($name_file, $name_base, $tt2)
{
<paula version="1.1">
<header paula_id="{$name_file}"/>
<markList xmlns:xlink="http://www.w3.org/1999/xlink" type="sentence_seg"
          xml:base="{$name_base}">
{
local:create-sentence-text($tt2)
}
</markList>
</paula>
};

declare function local:create-paula-mark-sent-text-conllu($name_file,
                                                               $name_base, $tt2)
{
local:create-sentence-text-conllu($tt2)
};

declare function local:create-paula-mark-sent-text-conllu2($name_file,
                                                               $name_base, $tt2)
{
local:create-sentence-text-conllu2($tt2)
};

declare function local:create-paula-mark-sent-xml($name_file, $name_base, $tt2)
{
<paula version="1.1">
<header paula_id="{$name_file}"/>
<markList xmlns:xlink="http://www.w3.org/1999/xlink" type="sentence_seg"
          xml:base="{$name_base}">
{
local:create-sentence($tt2)
}
</markList>
</paula>
};
(: end sentence-split related functions :)


if ($type = "xml-paula")

then

let $name_base := file:name($from)
let $name_file := replace($name_base, "xml$", "tok01")
let $tok :=
local:create-paula-mark-xml($from, $name_file, $name_base)
return
<output>{
local:create-paula-mark-xml-print($tok),
local:create-paula-mark-sent-xml($name_file || ".sentence_seg01", $name_file
                                                                || ".xml", $tok)
}</output>


else if ($type = "txt-paula")

then

let $name_base := file:name($from)
let $name_file := replace($name_base, ".txt$", "") || ".tok01"
let $primary := local:create-paula-primary-text-text($from,
                                             replace($name_base, "txt", "text"))
let $tok := local:create-paula-mark-text($primary, $name_file, $name_base)
return
(
<output>{
$primary,
$tok => local:create-paula-mark-text-print(),
local:create-paula-mark-sent-text(replace($name_base, ".txt$", "") ||
                                 ".tok01.sentence_seg01", $name_file || ".xml",
$tok)
}</output>
)

else if ($type = "txt-conllu")

then

let $name_base := file:name($from)
let $name_file := replace($name_base, "txt$", "tok01")
let $primary := local:create-paula-primary-text-text($from,
                                             replace($name_base, "txt", "text"))
let $tok := local:create-paula-mark-text($primary, $name_file, $name_base)
return
local:create-paula-mark-sent-text-conllu($name_file || ".sentence_seg01",
                                                     $name_file || ".xml", $tok)

else if ($type = "txt-conllu2")

then

let $name_base := file:name($from)
let $name_file := replace($name_base, "txt$", "tok01")
let $primary := local:create-paula-primary-text-text($from,
                                             replace($name_base, "txt", "text"))
let $tok := local:create-paula-mark-text($primary, $name_file, $name_base)
return
local:create-paula-mark-sent-text-conllu2($name_file || ".sentence_seg01",
                                                    $name_file || ".xml", $tok)
