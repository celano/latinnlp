# Verbator: A Latin Tokenizer and Sentence Splitter

Verbator is an efficient Latin tokenizer and sentence splitter for _TEI-Epidoc_ encoded files and _txt_ files (with an accuracy of 0.99990 as a tokenizer and 0.99769 as a sentence splitter [1]). It requires BaseX, a very lightweight XQuery processor (~10 MB), which is found in the basex directory. In the `example` folder, I provide texts to try out Verbator. Follow the instructions in the following sections.

## Local Install

Clone the repository, go into the `tokenize/tokenizer/basex/bin` directory, and run one of the following commands (with `${absolute_path}` containing your absolute path to the `tokenizer` directory):

1. `./basex -bfrom=${absolute_path}/example/phi0134.phi003.perseus-lat2.xml -btype=xml-paula ${absolute_path}/verbator.xq`
2. `./basex -bfrom=${absolute_path}/example/phi0448.phi001.perseus-lat2.txt -btype=txt-paula ${absolute_path}/verbator.xq`
3. `./basex -bfrom=${absolute_path}/example/phi0448.phi001.perseus-lat2.txt -btype=txt-conllu ${absolute_path}/verbator.xq`

The command `basex`, which is in the `bin` folder, is followed by the arguments:

- `-bfrom=`: this argument contains the absolute path to the directory containing (see files within the directory `example` for examples):
  - an **Epidoc-TEI-xml** encoded according to the PerseusDL schema [2]; additionally, it is required that the markup of `<choice/>` elements is **uniformed** thus: https://git.informatik.uni-leipzig.de/celano/latinnlp/-/tree/master/original-Latin-files  
  - a **txt file** without any markup
- `-btype=` : this argument specifies the output desired: **xml-paula** transforms an Epidoc-TEI-xml into paula; **txt-paula** transforms a txt file into paula; while **txt-conllu** transforms a txt file into the conllu format.  
- `${absolute_path}/verbator.xq` : this is the absolute path to the `verbator.xq` script.

Since relative paths with `basex` depend on a few system variables, use absolute paths to be sure to run the tokenizer. Moreover, note the combinations between type of file (`xml` or `txt`) and the values of the `-btype` arguments: they cannot be modified. 

## REST API

Verbator can also be queried online thus (@ is followed by the absolute or relative path to a file):

1. `curl -F "file=@phi0134.phi003.perseus-lat2.xml" http://pcai056.informatik.uni-leipzig.de:8984/xml-paula`
2. `curl -F "file=@phi0448.phi001.perseus-lat2.txt" http://pcai056.informatik.uni-leipzig.de:8984/txt-paula`
3. `curl -F "file=@phi0448.phi001.perseus-lat2.txt" http://pcai056.informatik.uni-leipzig.de:8984/txt-conllu`

Read the section above and see files in the `example` directory to see what the input should look like.

---
[1] See evaluation here https://git.informatik.uni-leipzig.de/celano/latinnlp/-/tree/master/tokenize/tokenizer/test <br/>
[2] http://epidoc.stoa.org/schema/latest/tei-epidoc.xml
