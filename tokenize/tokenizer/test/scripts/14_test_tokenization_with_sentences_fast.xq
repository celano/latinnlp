xquery version "3.1" encoding "utf-8"; (: BaseX 9.3.2 :)

(:This query is different from 01_test_tokenization_with_sentences_fast.xq
because it specifies "SpaceAfter=No" for tokens :)

declare option db:chop 'false';
(: the database contains all PerseusDL texts tokenized and sentence split :)
declare variable $mark := db:open("final")/paula[header
                                       [ends-with(@paula_id, "sentence_seg01")]]
                                       /markList/mark;

declare variable $num :=
  let $c := count($mark)
  let $t := round($c * 0.1)
  return
  random:seeded-integer(3, xs:integer($t), xs:integer($c));


<check>{
 for $t at $c2 in $num
 let $pre := $mark[$t - 1]
 let $u := $mark[$t]
 let $fol := $mark[$t + 1]
 let $base := $u/ancestor::*:markList/@*:base
 let $sent := replace($u/@*:href, "[()]", "")
              => replace("#", "") => tokenize(",")
 return
  <sentence n="{$c2}" file="{$base}">
  {
   element pre-sent {
   $pre/@id, $pre/following-sibling::comment()[2]/data() =>
   replace("&amp;#45;", "-")},
   element focus-sent {$u/@id, $u/following-sibling::comment()[2]/data() =>
   replace("&amp;#45;", "-")
   },
   element fol-sent {$fol/@id,$fol/following-sibling::comment()[2]/data() =>
   replace("&amp;#45;", "-")
   },
   <sent-tok>{
     let $n2 := count($sent)
     for $j at $c2 in $sent
     let $mark :=
     db:open("final")[tokenize(db:path(.), "/")[2] = $base]//mark[@id = $j]
     let $href1 := replace($mark/@*:href, "(.*'',)([0-9]+)(,)([0-9]+)(\)\))", "$2") => xs:integer()
     let $href2 := replace($mark/@*:href, "(.*'',)([0-9]+)(,)([0-9]+)(\)\))", "$4") => xs:integer()
     let $token := $mark/following-sibling::comment()[1]
     let $mark2 :=
     $mark/following-sibling::mark[1]
     let $href3 := if ($mark2) then replace($mark2/@*:href, "(.*'',)([0-9]+)(,)([0-9]+)(\)\))", "$2") => xs:integer() else -2
     let $href4 := replace($mark2/@*:href, "(.*'',)([0-9]+)(,)([0-9]+)(\)\))", "$4")
     return

      element t {attribute idt {$c2}, attribute space 
      {if ($c2 = $n2) then "_"
       else if
      ($href1 + $href2 = $href3) then "SpaceAfter=No" else "_"},

      if ($token/data() = "&amp;#45;") then "-"
      else
      $token/data()

                }
  }</sent-tok>
 }</sentence>
}</check>
