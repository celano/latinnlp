xquery version "3.1" encoding "utf-8"; (: BaseX 9.3.2 :)

let $verbator :=
json-doc("/home/technician/Documents/latinnlp/tokenize/tokenizer/test/texts" ||
"/05_sentences_verbator.json")

let $udpipe :=
json-doc("/home/technician/Documents/latinnlp/tokenize/tokenizer/test/texts" ||
"/05_sentences_udpipe-perseus.json")

return
array:join(
for $a in $verbator => array:flatten()
let $b := $udpipe => array:flatten()
where not($a?sentence = $b?sentence)
or not($a?start = $b?start)
or not($a?long = $b?long)
return
array {$a}) ! json:serialize(.)
