xquery version "3.1" encoding "utf-8"; (: BaseX 9.4.5 :)

(: This query has been used to check the UDPipe_perseus tokenization :)

declare variable $list :=
fetch:xml("https://git.informatik.uni-leipzig.de/celano/latinnlp/-/" ||
"raw/master/abbreviations/abbr-in-texts.xml")//a/@f;

let $r := fetch:xml("https://git.informatik.uni-leipzig.de/" ||
"celano/latinnlp/-/raw/master/tokenize/not-to-tokenize.xml")//@f
let $r2 := fetch:xml("https://git.informatik.uni-leipzig.de/" ||
"celano/latinnlp/-/raw/master/tokenize/to-tokenize.xml")//@f

let $m :=
json-doc("/home/technician/Documents/latinnlp/tokenize/" ||
"tokenizer/test/texts/12_perseus_diff_verbator_tokens.json") => array:flatten()

for $u at $c in $m
let $pre := $m[xs:integer(?idt) = xs:integer($u?idt) - 1 and
               ?id = $u?id]
let $foll := $m[xs:integer(?idt) = xs:integer($u?idt) + 1 and
               ?id = $u?id]
where
not(matches($u?form, "['?!;:&#x22;())].+"))

where 
not(matches($u?form, "que$") and not($u?form = $r))
where 
not(matches($u?form, "ue$") and not($u?form = $r))
where 
not(matches($u?form, "ve$") and not($u?form = $r))
where 
not(matches($u?form, "ne$") and not($u?form = $r))
where
not(matches($u?form, ".+['?!;:&#x22;())]"))

where
not(matches($u?form, ".+-"))
where
not(matches($u?form, "-.+"))
where
not(matches($u?form, "—.+"))
where
not(matches($u?form, ".+—"))
where
not(matches($u?form, "“.+"))
where
not(matches($u?form, "‘.+"))
where
not(matches($u?form, ".+”"))
where
not(matches($u?form, ",.+"))
where
not(matches($u?form, "\[.+"))
where
not(matches($u?form, ".+."))

(: the following where test for presence of abbreviations :)
where
not(
$u?form = ". " and 
(string-length($pre?form) = 1
or $pre?form || "." = $list )
)
where
not(
$u?form = "." and 
(string-length($pre?form) = 1
or $pre?form || "." = $list )
)
where
not(
(string-length($u?form) = 1 or $foll?form || "." = $list) and $foll?form = "."
)
where
not(
(string-length($u?form) = 1 or $foll?form || "." = $list) and $foll?form = ". "
)


return
$u


(: I have used the following queries to check the words ending in que, ue/ve, 
   and ne:)

(:
let $r := fetch:xml("https://git.informatik.uni-leipzig.de/" ||
"celano/latinnlp/-/raw/master/tokenize/not-to-tokenize.xml")//@f
let $r2 := fetch:xml("https://git.informatik.uni-leipzig.de/" ||
"celano/latinnlp/-/raw/master/tokenize/to-tokenize.xml")//@f

let $m :=
json-doc("/home/technician/Documents/latinnlp/tokenize/" ||
"tokenizer/test/texts/perseus_diff_verbator_tokens.json") => array:flatten()

for $u at $c in $m

where 
matches($u?form, "que$") and not($u?form = $r)
and
not(
db:attribute("morpheus_merged_in_basex_grouped_by_word_form", 
$u?form => replace("que$", "") => lower-case(),
"f")
)
return
$u
:)

(:
let $r := fetch:xml("https://git.informatik.uni-leipzig.de/" ||
"celano/latinnlp/-/raw/master/tokenize/not-to-tokenize.xml")//@f
let $r2 := fetch:xml("https://git.informatik.uni-leipzig.de/" ||
"celano/latinnlp/-/raw/master/tokenize/to-tokenize.xml")//@f

let $m :=
json-doc("/home/technician/Documents/latinnlp/tokenize/" ||
"tokenizer/test/texts/perseus_diff_verbator_tokens.json") => array:flatten()

for $u at $c in $m

where 
matches($u?form, "[^q][uv]e$") and not($u?form = $r)
and
not(
db:attribute("morpheus_merged_in_basex_grouped_by_word_form", 
$u?form => replace("que$", "") => lower-case(),
"f")
)
return
$u
:)

(:
let $r := fetch:xml("https://git.informatik.uni-leipzig.de/" ||
"celano/latinnlp/-/raw/master/tokenize/not-to-tokenize.xml")//@f
let $r2 := fetch:xml("https://git.informatik.uni-leipzig.de/" ||
"celano/latinnlp/-/raw/master/tokenize/to-tokenize.xml")//@f

let $m :=
json-doc("/home/technician/Documents/latinnlp/tokenize/" ||
"tokenizer/test/texts/perseus_diff_verbator_tokens.json") => array:flatten()

for $u at $c in $m

where 
matches($u?form, "ne$") and not($u?form = $r)
and
not(
db:attribute("morpheus_merged_in_basex_grouped_by_word_form", 
$u?form => replace("[^q]ue$", "") => lower-case(),
"f")
)
return
$u
:)