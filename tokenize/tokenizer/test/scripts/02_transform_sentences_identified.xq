xquery version "3.1" encoding "utf-8"; (: BaseX 9.3.2 :)

(: This query eliminates duplicate sentences in the test set :)

<check>{
for $my in
doc("/home/technician/Documents/latinnlp/tokenize/tokenizer/test/" ||
    "texts/01_sentences_identified.xml")
//sentence
group by $i := $my/focus-sent
return
 for $u in $my[1] (: this selects only the first of the duplicates :)
 return
 copy $b := $u
 modify(
   for $i in $b//t[. = "ne"]
   let $v := $i/following-sibling::t[1][. = "que" or . = "c"]
   where $v
   return
   (replace value of node $i with $i || $v,
   delete node $v)
 )
 return $b
}</check>
