xquery version "3.1" encoding "utf-8"; (: BaseX 9.3.2 :)

<check>{
for $my in
doc("/home/technician/Documents/latinnlp/tokenize/tokenizer/test/" ||
    "texts/01_sentences_identified2.xml")
//sentence
group by $i := $my/focus-sent
return
 for $u in $my[1] (: this selects only the first of the duplicates :)
 return
 copy $b := $u
 modify(
   for $i in $b//t[. = "ne" or . = "Ne"]
   let $v := $i/following-sibling::t[1][.= "que" or . = "c"]
   where $v
   return
   (replace value of node $i with $i || $v,
   replace value of node $i/@space with $v/@space,
   delete node $v)
 )
 return $b
}</check>
