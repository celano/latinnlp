# This script compares token offsets

import json
import numpy as np
import sklearn.metrics

perseus = "/home/technician/Documents/latinnlp/tokenize/tokenizer/test/texts/" \
"10_tokens_perseus.json"
verbator = "/home/technician/Documents/latinnlp/tokenize/tokenizer/test/texts/"\
"10_tokens_verbator.json"

with open(perseus, "r") as f:
    dataP = json.load(f)

with open(verbator, "r") as f:
    dataV = json.load(f)

mP = np.zeros(sum(map(lambda x: x["long"] if "SpaceAfter=No" in x["space"] else x["long"] + 1, dataP)), dtype=np.float32)
mV = np.zeros(sum(map(lambda x: x["long"] if "SpaceAfter=No" in x["space"] else x["long"] + 1, dataV)), dtype=np.float32)

def prepare_vector(data, vector):
    for i in data:
         vector[i["start"] + i["long"] - 1] = 1

prepare_vector(dataP, mP)
prepare_vector(dataV, mV)

tn, fp, fn, tp = sklearn.metrics.confusion_matrix(mP, mV).ravel()

sklearn.metrics.classification_report(mP, mV, output_dict=True)
