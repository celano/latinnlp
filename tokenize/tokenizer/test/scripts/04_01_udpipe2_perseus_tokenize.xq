xquery version "3.1" encoding "utf-8"; (: BaseX 9.3.2 :)

(: This query uses UDPipe as a parser for the test set :)

<check name="udpipe" model="latin-ittb-ud-2.6-200830">{
for $u at $c in

tokenize(

json:parse(
proc:execute("curl", ("-F", "data=@/home/technician/Documents/latinnlp/tokenize" ||
                      "/tokenizer/test/texts/" ||
                      "/03_sentences_identified_text_only.txt",
                      "-F", "model=latin-perseus-ud-2.6-200830",
                      "-F", "tokenizer=",
                      "http://lindat.mff.cuni.cz/services/udpipe/api/process")
            )
                      /output
        )//result

, "\n\n")

return

let $i :=  <sentence n="{$c}">{
            for $p in tokenize($u, "\n")
            return
            if (starts-with($p, "# text")) then
                               attribute original {replace($p, "# text = ", "")}
            else if (starts-with($p, "#")) then ()
            else

            let $j := tokenize($p, "\t")
            return
            <t space="{$j[10]}">{$j[2]}</t>
            }</sentence>
let $n := $i/@n
let $original := $i/@original

return
<sentence n="{$n}">
 <focus-sent>{$original/data()}</focus-sent>
 <sent-tok>
 {$i/t}
 </sent-tok>
</sentence>

}</check>
