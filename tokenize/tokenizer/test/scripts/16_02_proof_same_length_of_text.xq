xquery version "3.1" encoding "utf-8"; (: BaseX 9.3.2 :)

sum(
let $f := 
json-doc("/home/technician/Documents/latinnlp/tokenize/tokenizer/test/texts/11_tokens_verbator2.json")
=> array:flatten()

for $u at $c in $f

let $a :=$u?start
let $b :=$u?long
let $c := $f[$c + 1]?start

let $d := if ($a + $b != $c ) then $b + 1 else $b

return
$d
)

,

sum(
let $f := 
json-doc("/home/technician/Documents/latinnlp/tokenize/tokenizer/test/texts/11_tokens_udpipe-perseus.json")
=> array:flatten()

for $u at $c in $f

let $a :=$u?start
let $b :=$u?long
let $c := $f[$c + 1]?start

let $d := if ($a + $b != $c ) then $b + 1 else $b

return
$d
)
