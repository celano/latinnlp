xquery version "3.1" encoding "utf-8"; (: BaseX 9.4.5 :)

(: This query has been used to check the UDPipe_perseus tokenization :)

declare variable $list :=
fetch:xml("https://git.informatik.uni-leipzig.de/celano/latinnlp/-/" ||
"raw/master/abbreviations/abbr-in-texts.xml")//a/@f;

let $r := fetch:xml("https://git.informatik.uni-leipzig.de/" ||
"celano/latinnlp/-/raw/master/tokenize/not-to-tokenize.xml")//@f
let $r2 := fetch:xml("https://git.informatik.uni-leipzig.de/" ||
"celano/latinnlp/-/raw/master/tokenize/to-tokenize.xml")//@f

let $m :=
json-doc("/home/technician/Documents/latinnlp/tokenize/" ||
"tokenizer/test/texts/13_verbator_diff_perseus_tokens.json") => array:flatten()

for $u at $c in $m
let $pre := $m[xs:integer(?idt) = xs:integer($u?idt) - 1 and
               ?id = $u?id]
let $foll := $m[xs:integer(?idt) = xs:integer($u?idt) + 1 and
               ?id = $u?id]

where $u?form = "."
and not($pre?form || "que" = $list)
and not(string-length($pre?form) = 1)

where $pre?form="Ap"

return
<g focus="{$u?form}">{
$pre?form
}</g>

(:
declare variable $list :=
fetch:xml("https://git.informatik.uni-leipzig.de/celano/latinnlp/-/" ||
"raw/master/abbreviations/abbr-in-texts.xml")//a/@f;

let $r := fetch:xml("https://git.informatik.uni-leipzig.de/" ||
"celano/latinnlp/-/raw/master/tokenize/not-to-tokenize.xml")//@f
let $r2 := fetch:xml("https://git.informatik.uni-leipzig.de/" ||
"celano/latinnlp/-/raw/master/tokenize/to-tokenize.xml")//@f

let $m :=
json-doc("/home/technician/Documents/latinnlp/tokenize/" ||
"tokenizer/test/texts/13_verbator_diff_perseus_tokens.json") => array:flatten()

for $u at $c in $m
let $pre := $m[xs:integer(?idt) = xs:integer($u?idt) - 1 and
               ?id = $u?id]
let $foll := $m[xs:integer(?idt) = xs:integer($u?idt) + 1 and
               ?id = $u?id]

where $u?form = "que"
and not($pre?form || "que" = $r2)
and not($pre?form || "que" = $r )

where 
not(db:attribute("morpheus_merged_in_basex_grouped_by_word_form", $pre?form, "f"))



return
<g focus="{$u?form}">{
$pre?form
}</g>
:)

(:
declare variable $list :=
fetch:xml("https://git.informatik.uni-leipzig.de/celano/latinnlp/-/" ||
"raw/master/abbreviations/abbr-in-texts.xml")//a/@f;

let $r := fetch:xml("https://git.informatik.uni-leipzig.de/" ||
"celano/latinnlp/-/raw/master/tokenize/not-to-tokenize.xml")//@f
let $r2 := fetch:xml("https://git.informatik.uni-leipzig.de/" ||
"celano/latinnlp/-/raw/master/tokenize/to-tokenize.xml")//@f

let $m :=
json-doc("/home/technician/Documents/latinnlp/tokenize/" ||
"tokenizer/test/texts/13_verbator_diff_perseus_tokens.json") => array:flatten()

for $u at $c in $m
let $pre := $m[xs:integer(?idt) = xs:integer($u?idt) - 1 and
               ?id = $u?id]
let $foll := $m[xs:integer(?idt) = xs:integer($u?idt) + 1 and
               ?id = $u?id]

where $u?form = "ne"



return
<g focus="{$u?form}">{
$pre?form
}</g>
:)

(:
declare variable $list :=
fetch:xml("https://git.informatik.uni-leipzig.de/celano/latinnlp/-/" ||
"raw/master/abbreviations/abbr-in-texts.xml")//a/@f;

let $r := fetch:xml("https://git.informatik.uni-leipzig.de/" ||
"celano/latinnlp/-/raw/master/tokenize/not-to-tokenize.xml")//@f
let $r2 := fetch:xml("https://git.informatik.uni-leipzig.de/" ||
"celano/latinnlp/-/raw/master/tokenize/to-tokenize.xml")//@f

let $m :=
json-doc("/home/technician/Documents/latinnlp/tokenize/" ||
"tokenizer/test/texts/13_verbator_diff_perseus_tokens.json") => array:flatten()

for $u at $c in $m
let $pre := $m[xs:integer(?idt) = xs:integer($u?idt) - 1 and
               ?id = $u?id]
let $foll := $m[xs:integer(?idt) = xs:integer($u?idt) + 1 and
               ?id = $u?id]

where $u?form = "ue" (: change into ve:)



return
<g focus="{$u?form}">{
$pre?form
}</g>
:)