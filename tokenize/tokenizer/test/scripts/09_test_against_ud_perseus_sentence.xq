xquery version "3.1" encoding "utf-8"; (: BaseX 9.3.2 :)

(: this query creates json files for the sentences of 
   la_perseus-ud-test and those parsed by Verbator so that 
   their offsets can be compared in sklearn :)

declare variable $path :=  
"/home/technician/Documents/latinnlp/tokenize/"
 || "tokenizer/test/texts/la_perseus-ud-test.conllu";
 
declare function local:transform-ud($a) {
   for $u in tokenize($a, "\n\n")
   return
    array {
     for $y in tokenize($u, "\n")
     return
      if (matches($y, "^#"))
      then
      map {tokenize($y, "=")[1] : replace(tokenize($y, "=")[2], "^ ", "")}
      else
      let $k := tokenize($y, "\t")
      return
       map {"id" : $k[1], "form" : $k[2], "space" : $k[10]}
          }
                                      };
declare function local:extract-sentences-ud($a) {
  string-join(
  for $u in array:flatten($a)?("# text ")
  return
  $u, " ")  
  
};

declare variable $text := 
local:extract-sentences-ud(local:transform-ud(unparsed-text($path)))
;
(: length 62513 :)

declare function local:calculate-offset($position, $marks)
 {
     let $mark   := head($marks)
     let $position := if(count($position) = 0)
      then $mark
      else (
          $position,
          map {    "sentence" : $mark?sentence,
                   "start" :   let $o := $position[count($position)]?start
                                         + $position[count($position)]?long
                               return
                               if (substring($text, $o, 1) = " ") then
                               $o + 1 else $o,
                    "long" : $mark?long }
             )
     return (
        if(count($marks) > 1) then
          local:calculate-offset($position, tail($marks))
        else $position
      )
};

declare function local:sentence-split-verbator($a) {
  
 (file:write(file:temp-dir() || "text.txt", $a)
  ,
  http:send-request(
  <http:request method='POST' override-media-type='string'>
    <http:multipart media-type='multipart/form-data'>
      <http:header name='content-disposition'
        value='form-data; name="file"; filename="{ file:name($path) }"'/>
      <http:body media-type='application/octet-stream'/>
    </http:multipart>
  </http:request>,
  'http://pcai056.informatik.uni-leipzig.de:8984/txt-conllu',
  file:read-binary( file:temp-dir() || "text.txt" ))
  ,
  file:delete(file:temp-dir() || "text.txt")  
  )[2]
};


let $perseus := 
local:calculate-offset((),          
for $u in array:flatten( local:transform-ud(unparsed-text($path)) )?("# text ")
return
map {
"sentence": $u, 
"start" : 1,
"long" : string-length($u)
}
)

let $verbator-conllu :=
local:transform-ud(unparsed-text($path)) =>
local:extract-sentences-ud() =>
local:sentence-split-verbator() =>
convert:binary-to-string()


let $verbator :=
local:calculate-offset((),          
for $u in array:flatten(local:transform-ud($verbator-conllu) )?("# text ")
return
map {
"sentence": $u, 
"start" : 1,
"long" : string-length($u)
}
)


return
array:join(array {$verbator})
!
json:serialize(.)

(:
array:join(array {$perseus})
!
json:serialize(.)
:)
 