xquery version "3.1" encoding "utf-8"; (: BaseX 9.3.2 :)

(: This query adds ids for the tokens :)

declare variable $path :=
"/home/technician/Documents/latinnlp/tokenize/tokenizer/test/" ||
"texts/04_sentences_tagged_by_udpipe2_perseus.xml";


<check name="udpipe" model="latin-ittb-ud-2.6-200830">{
for $k in doc($path)//sentence
return
<sentence n="{$k/@n}">{
$k/focus-sent,
<sent-tok>
{for $g at $c2 in $k//t return 
element t {attribute idt {$c2}, $g/@*, $g/text()}}
</sent-tok>
}</sentence>
}
</check>
