declare variable $d := doc("/home/technician/Documents/latinnlp/tokenize/tokenizer/test/texts/02_sentences_identified_transformed.xml");

for $u in json-doc("/home/technician/Documents/latinnlp/tokenize/tokenizer/test/texts/08_udpipe_difference_verbator.json")
=> array:flatten()

where matches($u?sentence, "[\.:;?!]")
and 

(
not(matches($u?sentence, "[\.:;?!]."))
 
and

(
every $u2 in $d//focus-sent
satisfies not(contains($u2, $u?sentence || "’"))
)
and
(
every $u2 in $d//focus-sent
satisfies not(contains($u2, $u?sentence || "”"))
)
)
return


<g>
<s>{$u?sentence}</s>
{
for $u2 in $d//focus-sent
where contains($u2, $u?sentence)
return 
<verbator>{$u2/text() 
}</verbator>
}
</g>