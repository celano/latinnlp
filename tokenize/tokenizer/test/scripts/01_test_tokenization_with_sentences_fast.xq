xquery version "3.1" encoding "utf-8"; (: BaseX 9.3.2 :)

(: This queries identies sentences for a test set, chosen at random from
   all the texts :)

declare option db:chop 'false';
(: the database contains all PerseusDL texts tokenized and sentence split :)
declare variable $mark := db:open("final")/paula[header
                                       [ends-with(@paula_id, "sentence_seg01")]]
                                       /markList/mark;

declare variable $num :=
  let $c := count($mark)
  let $t := round($c * 0.1)
  return
  random:seeded-integer(3, xs:integer($t), xs:integer($c));


<check>{
 for $t at $c2 in $num
 let $pre := $mark[$t - 1]
 let $u := $mark[$t]
 let $fol := $mark[$t + 1]
 let $base := $u/ancestor::*:markList/@*:base
 let $sent := replace($u/@*:href, "[()]", "")
              => replace("#", "") => tokenize(",")
 return
  <sentence n="{$c2}" file="{$base}">
  {
   element pre-sent {
   $pre/@id, $pre/following-sibling::comment()[2]/data() =>
   replace("&amp;#45;", "-")},
   element focus-sent {$u/@id, $u/following-sibling::comment()[2]/data() =>
   replace("&amp;#45;", "-")
   },
   element fol-sent {$fol/@id,$fol/following-sibling::comment()[2]/data() =>
   replace("&amp;#45;", "-")
   },
   <sent-tok>{
     for $j in $sent
     let $mark :=
     db:open("final")[tokenize(db:path(.), "/")[2] = $base]//mark[@id = $j]
     let $token := $mark/following-sibling::comment()[1]
     return

      element t {

      if ($token/data() = "&amp;#45;") then "-"
      else
      $token/data()

                }
  }</sent-tok>
 }</sentence>
}</check>
