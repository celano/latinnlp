import json
from pyspark.sql import SparkSession
import re

spark = SparkSession \
    .builder \
    .appName("Python Spark SQL basic example") \
    .config("spark.some.config.option", "some-value") \
    .getOrCreate()

with open("/home/technician/Documents/latinnlp/tokenize/"
          "tokenizer/test/texts/11_tokens_verbator2.json") as f:
    dataV = json.load(f)

with open("/home/technician/Documents/latinnlp/tokenize/"
          "tokenizer/test/texts/11_tokens_udpipe-perseus.json") as f:
    dataP = json.load(f)

#for k in dataV:
#    for k2 in dataP:
#        if (k["start"] == k2["start"]) and (k["long"] == k2["long"]) and (k["form"] == k2["form"]):
#            intersect.append(k)

#dataV2 = [{"start" : x["start"], "long": x["long"], "form" : x["form"]} for x in dataV]
#dataP2 = [{"start" : x["start"], "long": x["long"], "form" : x["form"]} for x in dataP]
#intersect = []
#for k in dataV2:
#    if k in dataP2:
#        intersect.append(k)

dataVS = spark.read.json("/home/technician/Documents/latinnlp/tokenize/"
                "tokenizer/test/texts/11_tokens_verbator2.json", multiLine=True)
dataPS = spark.read.json("/home/technician/Documents/latinnlp/tokenize/"
           "tokenizer/test/texts/11_tokens_udpipe-perseus.json", multiLine=True)

dataVS.createOrReplaceTempView("verbator")
dataPS.createOrReplaceTempView("perseus")

intersect = spark.sql(
"SELECT * FROM verbator INNER JOIN perseus " 
"ON verbator.form=perseus.form and verbator.start=perseus.start and verbator.long=perseus.long")


print(len(dataV)) #638132
print(len(dataP)) #625278

dataVS.count() #638132
dataPS.count() #625278
intersect.count() #609118

verbator_dif_perseus = spark.sql(
"SELECT verbator.* FROM verbator LEFT JOIN perseus ON verbator.form=perseus.form "
"and verbator.start=perseus.start and verbator.long=perseus.long "
"WHERE perseus.form IS NULL or perseus.start IS NULL or perseus.long IS NULL")

verbator_dif_perseus.count() #29014 (i.e., 638132 - 609118)

perseus_dif_verbator = spark.sql(
"SELECT perseus.* FROM perseus LEFT JOIN verbator ON verbator.form=perseus.form "
"and verbator.start=perseus.start and verbator.long=perseus.long "
"WHERE verbator.form IS NULL or verbator.start IS NULL or verbator.long IS NULL")

perseus_dif_verbator.count() #16160 (.e., 625278 - 609118)

vv = verbator_dif_perseus.toPandas().to_dict("records")
pp = perseus_dif_verbator.toPandas().to_dict("records")

path2 = "/home/technician/Documents/latinnlp/tokenize/" \
         "tokenizer/test/texts/perseus_diff_verbator_tokens.json"
with open(path2, "w") as f:
    json.dump(pp, f, indent=4)

path3 = "/home/technician/Documents/latinnlp/tokenize/" \
         "tokenizer/test/texts/verbator_diff_perseus_tokens.json"
with open(path3, "w") as f:
    json.dump(vv, f, indent=4)