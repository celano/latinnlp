xquery version "3.1" encoding "utf-8"; (: BaseX 9.3.2 :)

let $verbator :=
json-doc("/home/technician/Documents/latinnlp/tokenize/tokenizer/test/texts" ||
"/05_sentences_verbator.json")

let $udpipe :=
json-doc("/home/technician/Documents/latinnlp/tokenize/tokenizer/test/texts" ||
"/05_sentences_udpipe-perseus.json")

for $a in $verbator => array:flatten()
for $b in $udpipe => array:flatten()
where $a?sentence = $b?sentence
and $a?start = $b?start
and $a?long = $b?long
return
$a
