# This script could be used if Verbator's output were considered the gold set,
# which is not.

import json
import numpy as np
import sklearn.metrics

perseus = "/home/technician/Documents/latinnlp/tokenize/tokenizer/test/texts/" \
"05_sentences_udpipe-perseus.json"
verbator = "/home/technician/Documents/latinnlp/tokenize/tokenizer/test/texts/"\
"05_sentences_verbator.json"

with open(perseus, "r") as f:
    dataP = json.load(f)

with open(verbator, "r") as f:
    dataV = json.load(f)

mP = np.zeros(sum(map(lambda x: x["long"] + 1 if x["fin"] == "y" else x["long"], dataP)), dtype=np.float32)
mV = np.zeros(sum(map(lambda x: x["long"] + 1 if x["fin"] == "y" else x["long"], dataV)), dtype=np.float32)

def prepare_vector(data, vector):
    for i in data:
         vector[i["start"] + i["long"] - 1] = 1

prepare_vector(dataP, mP)
prepare_vector(dataV, mV)

tn, fp, fn, tp = sklearn.metrics.confusion_matrix(mP, mV).ravel()

sklearn.metrics.classification_report(mP, mV, output_dict=True)
