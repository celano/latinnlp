xquery version "3.1" encoding "utf-8"; (: BaseX 9.3.2 :)

declare variable $path :=
"/home/technician/Documents/latinnlp/tokenize/"
 || "tokenizer/test/texts/02_sentences_identified_transformed2.xml";

 declare variable $path2 :=
"/home/technician/Documents/latinnlp/tokenize/"
 || "tokenizer/test/texts/04_sentences_tagged_by_udpipe2_perseus_with_id.xml";

declare function local:calculate-offset($position, $marks)
 {
     let $mark   := head($marks)
     let $position := if(count($position) = 0)
      then $mark
      else (
          $position,
          map {    "sentence" : $mark?sentence,

                   "start" :   let $o := $position[count($position)]?start
                                         + $position[count($position)]?long
                               return
                               if ($position[count($position)]?fin = "y") then 
                                   $o + 1 else $o

                    ,
                    "long" : $mark?long,
                    "pre-sent" : $mark?pre-sent,
                    "fol-sent" : $mark?fol-sent,
                    "id" :$mark?id,
                    "fin" : $mark?fin,
                    "form" : $mark?form,
                    "idt": $mark?idt

            })
     return (
        if(count($marks) > 1) then
          local:calculate-offset($position, tail($marks))
        else $position
      )
};

let $udpipe :=
local:calculate-offset((),
for $u in doc($path2)//focus-sent
return
for $t in $u//parent::*//t
return
map {
  "id": $u/parent::sentence/data(@n),
  "pre-sent" : $u/parent::sentence/pre-sent/data(),
  "sentence": $u/data(),
  "fol-sent" : $u/parent::sentence/fol-sent/data(),
  "start" : 1,
  "long" : string-length($t),
 
  "fin" : if ($t/@space= "SpaceAfter=No") then "n" else "y",
  "form" : $t/data(),
  "idt": $t/@idt/data()                                                  
}
)

let $verbator :=
local:calculate-offset((),
for $u in doc($path)//focus-sent
return
for $t in $u//parent::*//t
return
  map {
  "id": $u/parent::sentence/data(@n),
  "pre-sent" : $u/parent::sentence/pre-sent/data(),
  "sentence": $u/data(),
  "fol-sent" : $u/parent::sentence/fol-sent/data(),
  "start" : 1,
  "long" : string-length($t),
  "fin" : if ($t/@space= "SpaceAfter=No") then "n" else "y",
  "form" : $t/data(),
  "idt": $t/@idt/data()
  }
)

return
array {$verbator}
!
json:serialize(.)

(:
array {$udpipe}
!
json:serialize(.)
:)
