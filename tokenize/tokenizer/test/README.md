# Accuracy of Verbator

The database of the PerseusDL library contains 316 Latin texts 
(classical and non-classical). Verbator identifies 6,754,900 
tokens and 411,489 sentences. The accuracies measured on such a 
large variety of texts are likely to be currently 
the highest for a Latin sentence splitter/tokenizer:
**0.99769** as a sentence splitter and **0.99990** as a tokenizer (in 
the light of the errors found in the test set, the scripts have been corrected 
and the entire corpus has been parsed again, so the accuracies are likely to be even higher).

Two tests have been performed to evaluate Verbator's accuracy 
(both as a sentence splitter and tokenizer):

1. 1/10 of all sentences (**41,149**) has been randomly selected. 
After deleting sentence duplicates (**2,543**), the remaining sentences (**38,606**) 
have been used for testing. Verbator's output has been compared
with UDPipe's (Perseus treebank model), a performant sentence splitter and tokenizer [1]: 
the comparison is based on sentence/token _forms_ and _offsets_. 
If a sentence or a token is identified by both Verbator and UDPipe, 
the analysis is assumed to be correct. 
The set differences (Verbator − UDPipe_perseus
and UDPipe_perseus − Verbator) have been checked 
– because of the large amount of sentences/tokens –
semi-automatically (heuristics-based algorithms + manual check).

2. The file `la_perseus-ud-test.conllu` is used as a (gold) test set [2].

Most of the scripts used are in the `scripts` folder.

## Evaluation as a Sentence Splitter 

### Comparing Verbator's and UDPipe's outputs (no gold test set)

Verbator: **38,606** sentences <br/>
UDPipe_perseus: **37,546** sentences <br/>
Verbator ∩ UDPipe_perseus = **34,519** sentences <br/>
Verbator − UDPipe_perseus = **4,087** sentences: after semi-automatic check, **3998** were found to have been correctly identified by Verbator <br/>
UDPipe_perseus − Verbator = **3,027** sentences: after semi-automatic check, **130** were found to have been correctly identified by UDPipe_perseus<br/>

Verbator's accuracy can be estimated to be **0.99769** (i.e., (34,519+3,998)/38,606), 
while UDPipe_perseus's **0.92284** (i.e., (34,519+130)/37,546). 

Verbator's errors are due to few (questionable) abbreviations and the combinations .' or ." (and the ambiguous "Non." for the abbreviation "Nones" and the negation "Non" + full stop, which cannot be resolved without connsidering the context). UDPipe_perseus's errors are mostly due to missed abbreviations and inability to parse final punctuation marks + quotes.

### Using la_perseus-ud-test.conllu as a gold test set

| sentence-splitter | sentences identified |
| ---      |  ------  | 
| UD Perseus test file  | 939  | 
| Verbator  | 938  |

| | precision | recall | f1 |
|---|----|---|---|
| 0| 0.99989|0.99990 |0.99989 |
| 1| 0.99360|0.99255 |0.99307 |
| macro avg| 0.99674|0.99622|0.99648 |

Verbator turns out to have an accuracy of **0.99979**, if Perseus sentences are taken to be correct. However, the latter show 
the quote mark used uncorrectly, i.e., with a space before and after it. Moreover, a sentence is very questionably 
ended with a comma.

## Evaluation as a Tokenizer

### Comparing Verbator's and UDPipe's outputs (no gold test set)

Verbator: **638,132** sentences <br/>
UDPipe_perseus: **625,278** sentences <br/>
Verbator ∩ UDPipe_perseus = **60,9118** sentences <br/>
Verbator − UDPipe_perseus = **29,014** sentences: after semi-automatic check, **28,950** were found to have been correctly identified by Verbator<br/>
UDPipe_perseus − Verbator = **16,160** sentences: after semi-automatic check, **60** were found to have been correctly identified by UDPipe_perseus<br/>

Verbator's accuracy can be estimated as **0.99990** (e.g., (28,950+609,118)/638,132) [3]. 
Verbator missed a few abbreviations, such as "Ap.", and
misinterpreted e few final "ue"/"ne" (such as in "ungue" or "Africane"). 
UDPipe's accuracy can be estimated to be  **0.97425** (i.e., (60+609,118)/625,278) [3] [4].
UDPipe is essentially unable to parse "que", "ue/ve", "ne" and abbreviations. 

### Using la_perseus-ud-test.conllu as a gold test set

| tokenizer | tokens identified |
| ---      |  ------  | 
| UD Perseus test file  | 10,954  | 
| Verbator  | 11,007  |

| | precision | recall | f1 |
|---|----|---|---|
| 0| 1.0 |0.99897 |0.99949 |
| 1| 0.99518| 1.0 |0.99759 |
| macro avg| 0.99759|0.99949|0.99854 |

Verbator turns out to have an accuracy of **0.99915**.

---
[1] https://universaldependencies.org/conll18/results.html <br/>
[2] https://github.com/UniversalDependencies/UD_Latin-Perseus/blob/master/la_perseus-ud-test.conllu [accessed on 2021.01.06] <br/>
[3] If there were a tokenization error due to the underlying original text, I did not count it. <br/>
[4] If there were a token which could potentially be parsed in two different ways, such as "unicuique" vs. "uni"-"cuique", I counted UDPipe's tokenization ("unicuique") as correct.
