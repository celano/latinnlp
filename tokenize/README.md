# Verbator: A Rule-based Tokenizer and Sentence Splitter for Latin

This folder contains **Verbator**, a fast and efficient rule-based tokenizer and sentence splitter for (Classical and non-Classical) Latin, which 
identifies *morphosyntactic words* (i.e., the units for morphosyntactic
analysis) extracted from _Epidoc-TEI_ (PerseusDL) [1] or _txt_ files. The script is written in XQuery and relies on two *wordlists*:

* `to-tokenize.xml`
* `not-to-tokenize.xml`

The file `to-tokenize.xml` contains Latin wordforms which should always be
tokenized according to the analyses provided in that file. These words can be
considered 'special cases', whose automatic identification without a wordlist
would be complex. The file `non-to-tokenize.xml` contains word forms which should not be tokenized.

Tokenization of Latin poses a few challenges. For example,
there are a few word forms which are ambiguous 
without knowledge of the context: e.g., a few words ending in "-que", 
such as "quique", could either be or not be split.  

Read [02_tokenization.md](https://git.informatik.uni-leipzig.de/celano/latinnlp/blob/master/guidelines/02_tokenization.md)
for a detailed analyis of how exactly tokenization is performed.
In general, the following formal principles are followed to identify a 
morphosyntactic word: 

1. A morphosyntactic word typically corresponds to a string delimited 
by whitespaces
2. A whitespace-delimited string is further segmented when it ends with "que",
"ve"/"ue", and "ne".

Since not all words ending in "que", "ve"/"ue", and "ne" should be segmented, 
`non-to-tokenize.xml` is provided to filter these words.

On the other hand, `to-tokenize.xml` contains some  cases of 
word forms requiring tokenization. These segmentations have been identified 
when a wordform consists of free morphemes, i.e., morphemes that identify 
*stand-alone words* (e.g., secum = cum se). Only in a few cases has this principle 
not been followed, i.e., when a word form seems to be crystallized to such an 
extent as to represent a new word form (e.g., sicut, not anlyzed as *sic ut*). 
The identification of these cases may admittedly be rather arbitrary.

---
[1] The Tokenizer has been written to take the Epidoc-TEI Perseus files as an input. Note, however, that
it requires the markup of the `<choice/>` elements to be uniformed as shown at 
https://git.informatik.uni-leipzig.de/celano/latinnlp/-/tree/master/original-Latin-files
