# Normalization

This directory contains rules to create a normalization layer of annotation for Latin. The files are used by [1]. More information on the rationale for them is at [2].

---
[1] https://git.informatik.uni-leipzig.de/celano/latinnlp/-/blob/master/scripts/03.00_normalize_spelling.xq <br/>
[2] https://git.informatik.uni-leipzig.de/celano/latinnlp/-/blob/master/guidelines/01_orthography.md
