xquery version "3.1" encoding "utf-8"; (: BaseX 9.3.2 :)

(: written by Giuseppe G. A. Celano :)

declare namespace xlink="http://www.w3.org/1999/xlink";
declare namespace tei="http://www.tei-c.org/ns/1.0";
declare option db:chop 'false';
declare variable $data external;
declare variable $write external;

(: the following function serves to merge together
   those tokens which are separated: e.g.,
   te<add>m</add>tatas in phi0836.phi002.perseus-lat6 :)
declare function local:recursive($pre_marks, $marks)
  {
    let $mark   := head($marks)
    let $href2  := $pre_marks[last()]
    let $intsum := $href2/@start + $href2/@long
    let $pre_marks := if
                       (count($pre_marks) = 0)
                       then $mark

                       else if
                       ($href2/@href = $mark/@href and
                        $intsum  = $mark/@start
                        and not($mark[@m = "cabbr"])
                                                (: the func is applied
                                                         after cabbr are
                                                         separated
                                                :)

                        and not($mark[@m = "punct"])
                        and not($mark[@m = "foreign"]))
                       then
                       (
                       let $y := count($pre_marks)
                       let $c := $pre_marks[position() < $y]
                       return
                       (
                       $c,
                       element mark {attribute href {$href2/@href},
                                     attribute start {$href2/@start},
                                     attribute long {$href2/@long + $mark/@long
                                     }
                       ,
                        $mark/(@* except (@href, @start, @long)),
                        $href2/text() || $mark/text()
                       })
                       )
                       else
                       ($pre_marks,$mark)
      return (
        if(count($marks) > 1) then
          local:recursive($pre_marks, tail($marks))
        else $pre_marks
      )
  };

declare variable $abb :=
 fetch:xml(
  "https://git.informatik.uni-leipzig.de/celano/latinnlp/raw/master/" ||
  "abbreviations/abbr-in-texts.xml"
         );

declare variable $abbS :=
 for $i in $abb//a
 return
  map {"f" : data($i/@f)}?f ;

declare variable $to-tokenize :=
 fetch:xml(
  "https://git.informatik.uni-leipzig.de/celano/latinnlp/raw/master/" ||
  "tokenize/to-tokenize.xml"
          );

declare variable $to-tokenizeS :=
 for $o in $to-tokenize//w
 let $t1 := $o/t[1]
 let $t2 := $o/t[2]
 let $t3 := $o/t[3]
 return
  map
   {"f" : $o/@f/data(),
    "t1" : map {"f" : data($t1/text()), "s" : data($t1/@s), "l": data($t1/@l)},
    "t2" : map {"f" :  data($t2/text()), "s" : data($t2/@s), "l": data($t2/@l)},
    "t3" :
      if ($t3)
      then map {"f" :  data($t3/text()), "s" : data($t3/@s), "l": data($t3/@l)}
      else ()
   };

declare variable $to-tokenizeS1 :=
 $to-tokenizeS?f;

declare variable $not-to-tokenize :=
 fetch:xml(
  "https://git.informatik.uni-leipzig.de/celano/latinnlp/raw/master/" ||
  "tokenize/not-to-tokenize.xml"
          );

declare variable $not-to-tokenizeS :=
 for $i in $not-to-tokenize//w
 return
  map {"f" : data($i/@f), "pn" : data($i/@pn)};

declare variable $not-to-tokenizeS1 :=
 $not-to-tokenizeS?f;

declare variable $not-to-tokenizeS2 :=
 $not-to-tokenizeS1 ! replace(., "v", "u");

declare  function local:ws-tok($a)
{
(: the following let clause identifies the CTS structure which is already
   specified in each TEI/EPIDOC file: this means that any part of the XML file
   which is not the main text of a work is filtered out  :)

 let $cRefPattern := $a/tei:TEI/tei:teiHeader/tei:encodingDesc
                    /tei:refsDecl[@n="CTS"]/tei:cRefPattern
 let $l := count($cRefPattern) (: this is useful for creating token ids :)
 let $p := $cRefPattern[1]/@replacementPattern
          => replace("(#xpath\()(.*)(\))", "$2")
          => replace("tei", "*")
          => replace("\[.*?\]", "")

 for $text in xquery:eval($p, map { '': $a  })
 let $path := path($text)
 return
  let $e:=  element {"div"}
                    {attribute {"path"} {$path},
                     for $u in $text//text()
                     return
                     element {$u/parent::*/name()}
                     {attribute anc {$u/ancestor::*/name()},
                      fn:analyze-string($u, "\s")}
                    }

(: the following for clause is the point where the content of some elements
   can be filtered out :)

(: some elements, such as, for example, "listPerson"
   are automatically filtered because
   they do not fall within the scope of the xpath defined
   by the tei:cRefPattern, while others are because
   they are present in already filtered-out elements
   (e.g., "lem" always in "app").
:)

  for $tok at $c in $e//fn:non-match
               [not(matches(ancestor::*/@anc, "note"))]
               [not(matches(ancestor::*/@anc, "app"))]
               [not(matches(ancestor::*/@anc, "bibl"))]
               [not(matches(ancestor::*/@anc, "del"))]
               [not(matches(ancestor::*/@anc, "desc"))]
               [not(matches(ancestor::*/@anc, "docAuthor"))]
               [not(matches(ancestor::*/@anc, "figure"))]
               [not(matches(ancestor::*/@anc, "ref"))]
               [not(matches(ancestor::*/@anc, "speaker"))]
               [not(matches(ancestor::*/@anc, "stage"))]


  let $pos := sum($tok/(preceding::fn:match union preceding::fn:non-match)
                                                              /string-length(.))
  let $length := string-length($tok)
  return

  if       ($tok/ancestor::*:sic[ends-with(@anc, "choice sic")])
  then
   <mark href="{replace($e/@path, "Q\{http://www.tei-c.org/ns/1.0\}", "*:")}"
       start="{$pos + 1}" long="{$length}" lev="{$l}" m="csic"
       anc="{$tok/ancestor::*/@anc}">{$tok/text()}</mark>

  else if  ($tok/ancestor::*:corr[ends-with(@anc, "choice corr")])
  then
  <mark href="{replace($e/@path, "Q\{http://www.tei-c.org/ns/1.0\}", "*:")}"
       start="{$pos + 1}" long="{$length}" lev="{$l}" m="ccorr"
       anc="{$tok/ancestor::*/@anc}">{$tok/text()}</mark>

  else if ($tok/ancestor::*:abbr[ends-with(@anc, "choice abbr")])
  then
  <mark href="{replace($e/@path, "Q\{http://www.tei-c.org/ns/1.0\}", "*:")}"
       start="{$pos + 1}" long="{$length}" lev="{$l}" m="cabbr"
       anc="{$tok/ancestor::*/@anc}">{$tok/text()}</mark>

  else if ($tok/ancestor::*:expan[ends-with(@anc, "choice expan")])
  then
  <mark href="{replace($e/@path, "Q\{http://www.tei-c.org/ns/1.0\}", "*:")}"
       start="{$pos + 1}" long="{$length}" lev="{$l}" m="cexpan"
       anc="{$tok/ancestor::*/@anc}">{$tok/text()}</mark>

  else if ($tok/ancestor::*:reg[ends-with(@anc, "choice reg")])
  then
  <mark href="{replace($e/@path, "Q\{http://www.tei-c.org/ns/1.0\}", "*:")}"
       start="{$pos + 1}" long="{$length}" lev="{$l}" m="creg"
       anc="{$tok/ancestor::*/@anc}">{$tok/text()}</mark>

  else if ($tok/ancestor::*:orig[ends-with(@anc, "choice orig")])
  then
  <mark href="{replace($e/@path, "Q\{http://www.tei-c.org/ns/1.0\}", "*:")}"
       start="{$pos + 1}" long="{$length}" lev="{$l}" m="corig"
       anc="{$tok/ancestor::*/@anc}">{$tok/text()}</mark>

  else if ($tok/ancestor::*:foreign[ends-with(@anc, "foreign")])
  then
  <mark href="{replace($e/@path, "Q\{http://www.tei-c.org/ns/1.0\}", "*:")}"
       start="{$pos + 1}" long="{$length}" lev="{$l}" m="foreign"
       anc="{$tok/ancestor::*/@anc}">{$tok/text()}</mark>

  else if ($tok/ancestor::*:add[ends-with(@anc, "add")])
  then
  <mark href="{replace($e/@path, "Q\{http://www.tei-c.org/ns/1.0\}", "*:")}"
       start="{$pos + 1}" long="{$length}" lev="{$l}" m="add"
       anc="{$tok/ancestor::*/@anc}">{$tok/text()}</mark>

  else if (string-length($tok) < 3 and matches($tok, "\p{P}"))
  then
  <mark href="{replace($e/@path, "Q\{http://www.tei-c.org/ns/1.0\}", "*:")}"
       start="{$pos + 1}" long="{$length}" lev="{$l}" m="punct"
       anc="{$tok/ancestor::*/@anc}">{$tok/text()}</mark>

  else
   <mark href="{replace($e/@path, "Q\{http://www.tei-c.org/ns/1.0\}", "*:")}"
       start="{$pos + 1}" long="{$length}" lev="{$l}"
       anc="{$tok/ancestor::*/@anc}">{$tok/text()}</mark>

};

declare function local:manage-tag($a)
{
 for $m at $c in $a
 return
  if ($m[@m ="csic"]) then ()

  else if ($m[@m ="cexpan"]) then ()

  (: I assume the case percontatu's :)
  else if ($m[@m ="cabbr"] and matches($m, "\p{L}'\p{L}"))
  then

  let $j := tokenize($m, "'")
  return
  (
  element mark {$m/@href, $m/@start, attribute long {fn:string-length($j[1])},
                $m/@lev, $m/@m, $m/@anc, $j[1]},
  element mark {$m/@href, attribute start {$m/@start + fn:string-length($j[1])},
                attribute long {1+fn:string-length($j[2])},
                $m/@lev, $m/@m, $m/@anc, "'" || $j[2] }
  )

  else if ($m[@m ="corig"]) then ()

  else $m
};

declare function local:merge-add($a)
{
  local:recursive((), $a)
};

declare function local:avoid-abbr($a)
{
 for $t in $a
 let $s := $t[matches(., "^\p{Lu}{1}\.$")]
 let $b := $t[. = $abbS]
 let $d := $t[@m="cabbr"]
 return
  if ($s or $b or $d) then $t else
   for $tok in fn:analyze-string($t, "\p{P}")/*
   let $pos := sum($tok/preceding-sibling::*/text()/string-length(.))
   let $length := string-length($tok)
   return
    <mark href="{$t/@href}"
          start="{$t/@start + $pos}" long="{$length}"
          lev="{$t/@lev}">{$t/@m,
          $tok/text()}</mark>
};

declare  function local:to-tok($a)
{
 for $a in $a
 let $p := $to-tokenizeS
 let $b := $a[. = $to-tokenizeS1]

 return
  if ($b) then
   for $h in $p[?f = $a]?('t1', 't2', 't3')
   return
    <mark href="{$a/@href}" start="{$a/@start + $h?s -1}"
          long="{$h?l}" lev="{$a/@lev}">{$a/@m,  $h?f}</mark>
  else $a
};

declare function local:que-ve-ne($a)
{
 for $j in $a
 let $o := replace($j, "j", "i")
           => replace("J", "I")
           => normalize-unicode('NFD')   (: to avoid accented words in poetry:)
           => replace('\p{IsCombiningDiacriticalMarks}', '')
 let $a := $o[. = $not-to-tokenizeS1]
 let $q := matches($o, ".*.cumque$") or matches($o, ".*.cúmque$")
 let $n := matches($o, ".*.cunque$")
 let $v := matches($o, ".*.quomque$")
 let $h := matches($o, "one$|ine$|óne$|íne$")
 let $e := fn:analyze-string($j, "que$|ve$|ue$|ne$")
 return
  if (not($e/fn:match) or $a or $q or $n or $v or $h)
   then $j else
    for $tok in $e/*
    let $pos := sum($tok/preceding-sibling::*/text()/string-length(.))
    let $length := string-length($tok)
    return
     <mark href="{$j/@href}"
            start="{$j/@start + $pos}" long="{$length}"
            lev="{$j/@lev}">{
              $j/@m, $tok/text()}</mark>
};

declare function local:transform-mark($a)
{
 for $m in $a
 let $g := $m/@href
 group by $g
 return
  for $t in $m
  return
   (<mark
     xlink:href=
      "{'#xpointer(string-range(' || $t/@href || ',&apos;&apos;,' ||
      $t/@start || ',' || $t/@long || '))'}"
     com ="{data($t)}"
    >{$t/@m}</mark>
   )
};

(: if element names such as "add" or "sic" need to be printed add
   $m/@m :)
declare function local:finalize($a)
{
  for $m in $a
  count $c
  return
  (element mark {attribute id {"tok_" || $c}, $m/@*:href}, (: <= $m/@m :)
  if (matches($m/@com, "-")) then
  <!--&#45;--> else comment {data($m/@com)})
};

declare function  local:create-paula-mark($path_file, $name_file, $name_base)
{
 <paula version="1.1">
 <header paula_id="{$name_file}"/>
 <markList xmlns:xlink="http://www.w3.org/1999/xlink" type="tok"
          xml:base="{$name_base}">
{
 doc($path_file) => local:ws-tok() => local:avoid-abbr() =>
 local:to-tok() => local:que-ve-ne() => local:transform-mark()
 => local:finalize()
}
 </markList>
 </paula>
};

declare function local:write-paula-mark($target_to_write, $path_file,
                                        $name_file, $name_base)
{
 file:write(
 $target_to_write,
 <paula version="1.1">
 <header paula_id="{$name_file}"/>
 <markList xmlns:xlink="http://www.w3.org/1999/xlink" type="tok"
          xml:base="{$name_base}">
 {
  doc($path_file) => local:ws-tok() => local:manage-tag() => local:merge-add()
=> local:avoid-abbr() =>
 local:to-tok() => local:que-ve-ne() => local:transform-mark()
 => local:finalize()
 }
 </markList>
 </paula>,
 map {'method': 'xml', "omit-xml-declaration" :"no",
      "standalone": "no", "doctype-system": "../../paula_mark.dtd"
     }
           )
};

(: $data is the absolute pathname of a file to tokenize :)
(: $write is the directory where to create a directory and save the file :)

let $filename := replace(file:name($data), ".xml", "")
let $dir := $write
return
  (
   file:create-dir($dir || $filename)
   ,
   file:copy($data, $dir || $filename)
   ,
   local:write-paula-mark($dir || $filename || "/" ||
    $filename || ".tok01.xml",
    $data,
    $filename || ".tok01" ,
    file:name($data)
                         )
  )

(: use the following command with parallel :)
(:
find /home/technician/Documents/latinnlp/original-Latin-files/canonical-latinLit-0.0.403/ \
-name *lat[[:digit:]].xml \
| parallel "/home/technician/Documents/basex/bin/basex \
-bdata={} \
-bwrite=/home/technician/Documents/dfg_revising_standardizing_expanding/\
paula/latin/final2/ \
/home/technician/Documents/latinnlp/scripts/01.01_paula_tokenize.xq"
:)

(:
declare variable $path := "/home/technician/Documents/" ||
            "dfg_revising_standardizing_expanding/paula/latin/" ||
            "canonical-latinLit-0.0.403/data/";

for $f in file:descendants($path)[matches(., "(-lat)(\p{N}*)(\.)(xml)$")]
let $filename := replace(file:name($f), ".xml", "")
let $dir := "/home/technician/Documents/" ||
            "dfg_revising_standardizing_expanding/paula/latin/final/"
return

(
 file:create-dir($dir || $filename)
 ,
 file:copy($f, $dir || $filename)
 ,
 local:write-paula-mark($dir || $filename || "/" ||
  $filename || ".tok01.xml",
  $f,
  $filename || ".tok01" ,
  file:name($f)
                       )
)
:)
