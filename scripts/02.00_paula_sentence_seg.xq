xquery version "3.1" encoding "utf-8"; (: BaseX 9.3.2 :)

(: written by Giuseppe G. A. Celano :)

declare namespace xlink="http://www.w3.org/1999/xlink";
declare option db:chop 'false';
declare variable $file external;


declare function local:create-sentence($tt2)
{

for tumbling window $s in $tt2//mark
start  $a when fn:true()

only end $b previous $v next $r when


(: the following criteria aims to capture cases such as, e.g. ]. or .] :)

(
$b/matches(following-sibling::comment()[1], "[\p{Pe}\p{Pf}&#39;&#34;]")
and
$v/matches(following-sibling::comment()[1], "^[\.?!:;]") (: ^ to skip C. :)
)
or
(
$b/matches(following-sibling::comment()[1], "^[\.?!:;]") (: ^ to skip C. :)
and
not(
$r/matches(following-sibling::comment()[1], "[\p{Pe}\p{Pf}&#39;&#34;]")
   )
)

count $r2
return
(<mark id="{"clause_" || $r2}"
      xlink:href="{
                 "(" ||
                 string-join(
                 for $m in $s/@id/data()
                 return
                 "#" || $m, ",")
                 || ")"
                 }"
/>,


(: the following comment reconstructs the  sentence just putting together the
   tokens, each of them being separated, by default, by a space :)
(: parse-xml-fragment converts -&#45; into - :)
   comment{

    $s/(following-sibling::comment()[1])

   },

(: the following comment reconstructs the graphic sentence with the following
   logic: if the XPath expression of a token and that of the immediately
   following one is the same AND if the latter starts immediately after the
   former then the tokens are univerbated (i.e., this happens with punctuation
   marks). In all the other cases I add a space between the tokens by default:
   i.e. I assume that if the XPath expressions are different or there is some
   character between two tokens than this should be a space, since anything else
   between them does not belong to the sentence syntactically analyzed. This
   therefore means that in the original text there might be word(s) between
   two tokens :)

    comment {


    string-join(

    for $i at $c in $s
    let $f := fn:analyze-string($i/@xlink:href,
              "(#xpointer\(string-range\()(.*)(,'',)(.*)(,)(.*)(\)\))")
    let $i2 := $s[position() = $c + 1]
    let $f2 := fn:analyze-string($i2/@xlink:href,
              "(#xpointer\(string-range\()(.*)(,'',)(.*)(,)(.*)(\)\))")
    return

    (: the following if is for the last token which ends the sentence without
       anything else following :)
    if ($i/@id = $s[last()]/@id) then ($i/following-sibling::comment())[1]  => replace(" ", "")

    else
    if ($f//fn:group[@nr="2"] = $f2//fn:group[@nr="2"]
        and
        $f//fn:group[@nr="4"] + $f//fn:group[@nr="6"] = $f2//fn:group[@nr="4"]
       )
    then ($i/following-sibling::comment())[1]
    else ($i/following-sibling::comment())[1] || " "

    , "")

      }

    (: the following comment reconstructs the sentence by querying the original
       file, including the characters between two tokens: this means that
       anything between two tokens is printed out, even if it is not part of
       the syntactic sentence  :)



  )
};

declare function local:create-paula-mark-sent($name_file, $name_base, $tt2)
{
<paula version="1.0">
<header paula_id="{$name_file}"/>
<markList xmlns:xlink="http://www.w3.org/1999/xlink" type="sentence-seg"
          xml:base="{$name_base}">
{
local:create-sentence($tt2)
}
</markList>
</paula>
};

declare function local:write-paula-mark-sent($target_to_write,
                                        $name_file, $name_base, $tt2)
{
file:write(
$target_to_write,
<paula version="1.0">
<header paula_id="{$name_file}"/>
<markList xmlns:xlink="http://www.w3.org/1999/xlink" type="sentence-seg"
          xml:base="{$name_base}">
{
local:create-sentence($tt2)
}
</markList>
</paula>
,
map {'method': 'xml', "omit-xml-declaration" :"no",
      "standalone": "no", "doctype-system": "../../paula_mark.dtd"
    }
)
};

let $d := file:parent($file)
let $filename := file:name($file)
let $name2 := replace($filename, "tok01.xml", "tok01_sentence-seg01")
return
  local:write-paula-mark-sent($d || $name2 || ".xml",
  $name2
  ,
  $filename,
  doc($file)
  )

(:
find /home/technician/Documents/latinnlp/texts/parsed-texts/ -name *tok01.xml \
| parallel "/home/technician/basex946/bin/basex \
-bfile={} /home/technician/Documents/latinnlp/scripts/02.00_paula_sentence_seg.xq"
:)

(:
let $path := "/home/technician/Documents/dfg_revising_standardizing_expanding/paula/latin/final/"
for $d in file:children($path)
 return
 for $f in file:children($d)[ends-with(., "tok01.xml")]
  return

  (
  let $filename := file:name($f)
  let $name2 := replace($filename, "tok01.xml", "tok01_sentence-seg01")
  return
  local:write-paula-mark-sent($d || $name2 || ".xml",

  $name2
  ,
  $filename,

  doc($f)

  )
  )
:)

(:
local:create-paula-mark-sent(

"phi0448.phi001.perseus-lat2.tok01_sentence-seg01"
,
"phi0448.phi001.perseus-lat2.tok01.xml"
)
:)
