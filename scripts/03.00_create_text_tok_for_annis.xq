xquery version "3.1" encoding "utf-8"; (: BaseX 9.3 :)

(: written by Giuseppe G. A. Celano :)

declare namespace xlink="http://www.w3.org/1999/xlink";

declare variable $file external;

declare function local:create-text($file) {
  <paula version="1.1">
    <header paula_id="{
    file:name($file => replace(".tok01.xml", ""))
    }" type="TEXT"></header>
    <body>{string-join(doc($file)//comment(), " ")}</body>
  </paula>
};

(: the following function is to calcultate offsets if the original file is txt:)
declare function local:calculate-offset($position, $marks)
 {
     let $mark   := head($marks)
     let $position := if(count($position) = 0)
      then (<mark start="1" 
                  long="{xs:integer(string-length($mark))}">{$mark/text()}
            </mark>)
      else (
          $position,
          element mark { attribute start 
                                  {xs:integer($position[count($position)]/@start
                                   + $position[count($position)]/@long)},
                         attribute long {string-length($mark)}, $mark/text()}
             )
     return (
        if(count($marks) > 1) then
          local:calculate-offset($position, tail($marks))
        else $position
      )
};

(: this outputs a text tokenized without whitespaces, with offsets :)
declare function local:text($a) {
<r id="{$a//@paula_id || ".tok01"}"
   base="{$a//@paula_id || ".xml"}">
{for $u in $a//body
return
local:calculate-offset((), analyze-string($u, "\s+")//*)
[not(matches(., "\s+"))]
}</r>
};

declare function local:finalize-text-print($a)
{
 <r id="{$a/@id}" base="{$a/@base}">{
  for $m at $c in $a
  return
  (element mark {attribute id {"tok_" || $c},
           attribute xlink:href {"#xpointer(string-range(//body,''," ||
                                 $m/@start || "," || $m/@long || "))"}
                                },
  if (matches($m/text(), "-")) then
  <!--&#45;--> else comment {data($m/text())})
  }</r>
};

declare function local:create-paula-mark-text-print($a)
{

 <paula version="1.1">
 <header paula_id="{$a/@id}"/>
 <markList xmlns:xlink="http://www.w3.org/1999/xlink" type="tok"
           xml:base="{$a/@base}">
{
 local:finalize-text-print($a//mark)/(* union comment())
}
 </markList>
 </paula>
};


let $text1 := local:create-text($file)
return
(
file:write(replace($file, "tok01", "text_derived"), $text1,
               map {'method': 'xml', "omit-xml-declaration" :"no",
               "standalone": "no", "doctype-system": "../../paula_text.dtd"
                   }
          ),

file:write(
  replace($file, "tok01", "tok01_derived"),
  local:create-paula-mark-text-print(local:text($text1)),
        map {'method': 'xml', "omit-xml-declaration" :"no",
      "standalone": "no", "doctype-system": "../../paula_mark.dtd"
            }
          )
)

(: use the following with gnu parallel :)
(:
find /home/technician/Documents/dfg_revising_standardizing_expanding/\
paula/latin/final2/ -name *tok01.xml \
| parallel "/home/technician/basex946/bin/basex \
-bfile={} \
/home/technician/Documents/latinnlp/scripts/03.00_create_text_tok_for_annis.xq"
:)

(:
To use these file for Annis, one has to then delete the original *lat2.xml file
and *tok01.file and give their names to the text_derived.xml and 
tok01.derived.xml files:

find ~/Desktop/texts3/parsed-texts -name *lat\?.xml -delete
find ~/Desktop/texts3/parsed-texts -name *tok01.xml -delete

let $d := "/home/technician/Desktop/texts3/parsed-texts/"
for $u in file:list($d, true())[ends-with(., "tok01_derived.xml")]
return
file:move($d || $u, $d || replace($u, "tok01_derived", "tok01"))


let $d := "/home/technician/Desktop/texts3/parsed-texts/"
for $u in file:list($d, true())[ends-with(., "text_derived.xml")]
return
file:move($d || $u, $d || replace($u, ".text_derived", ""))
:)
