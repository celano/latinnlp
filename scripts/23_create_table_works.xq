xquery version "3.1" encoding "utf-8"; (: BaseX 9.4.6 :)

<table>{
 <tr>
    <th>cts</th>
    <th>author</th>
    <th>title</th>
 </tr>,
 for $j in doc("/home/technician/Documents/latinnlp/texts/list_works.xml")//work
 return
 <tr>
 <td width="250">{
"[" || $j/cts/data() || "]" || "(" || 'https://git.informatik.uni-leipzig.de/celano/latinnlp/-/tree/master/texts/opa/' || $j/cts || ')'
}</td>
 <td>{$j/author/data()}</td>
 <td>{$j/titleId/data()}</td> 
 </tr>
}</table>