from pathlib import Path
import requests
from lxml import etree
import json
import lxml.html

main_dir = "/home/technician/Documents/latinnlp/paula/ola"
dir_names = Path(main_dir).glob("*/*lat[1-9].xml")
url ="https://catalog.perseus.org/?utf8=✓&search_field=urn&q="


list_resolved_cts = []
for n, f in enumerate(dir_names):
    #if n == 55:
        resolved_cts = {}
        file_name = f.parts[7]
        f_split = file_name.split(".")
        f_cts = ".".join(f_split[0:2])
        html = requests.get(url +  f_cts).text
        docs = lxml.html.document_fromstring(html).xpath(".//div[@class='document ']")
        title = docs[0].xpath(".//h5/a")[0].text
        i = docs[0].xpath(".//dd")
        if i[0].text == "urn:cts:latinLit:" + f_cts:
               author = i[1].text
               resolved_cts["cts"] = file_name
               resolved_cts["author"] = author
               resolved_cts["title"] = title
               list_resolved_cts.append(resolved_cts)
               print(" ".join([file_name,author,title]))

with open("/home/technician/Documents/latinnlp/cts_urn/cts_urn_author_work.json", "w") as f:
            json.dump(list_resolved_cts, f, indent=3)
