xquery version "3.1" encoding "utf-8"; (: BaseX 9.3.2 :)

(: written by Giuseppe G. A. Celano :)

declare namespace xlink="http://www.w3.org/1999/xlink";
declare option db:chop 'false';
declare variable $tc :=
                   fetch:xml("https://git.informatik.uni-leipzig.de/celano/" ||
                   "latinnlp/-/raw/master/normalization/" ||
                   "tokens-with-initial-capital-letter.xml");
declare variable $tc2 :=
 for $i in $tc//t
 return
  map {"f" : data($i)}?f ;

declare variable $ru :=
                   fetch:xml("https://git.informatik.uni-leipzig.de/celano/" ||
                   "latinnlp/-/raw/master/normalization/" ||
                   "normalization-rules.xml");
declare variable $ru2 :=
 for $i in $ru//r
 return
  map {"m" : data($i/a[@type="match"]), "s": data($i/a[@type="substitute"])};

(: This query is to normalize word forms which can have diacritics and,
   more challengingly, abbreviations which are expanded in the original
   Epidoc-TEI texts.
:)

declare function local:decapitalize($a) {
  if
  (
  (: if the token has all capital letters it is left as it is :)
   every $c in string-to-codepoints($a)
   satisfies matches(codepoints-to-string($c), "\p{Lu}")
  )
  then $a
  (: if the token is in the list it is assumed to normally have initial capital
     letter
   :)
  else if ($a = $tc2)
  then $a
  (: if the token has initial capital letter, it is due to a final
     punctuation mark
   :)
  else
   if (matches($a, "V[bcdfghklmnpqrstvxyz]"))
   then replace(lower-case($a), "^.", "u")
   else
   lower-case($a)
};

declare function local:treat-j($a) {
  replace($a, "j", "i") =>
  replace("J", "I")
};

declare function local:treat-u($a) {
  replace($a, "([aeiou])(u)([aeiou])", "$1v$3") =>
  replace("^(u)([aeiou])", "v$2") =>
  replace("^U", "V")
};

declare function local:translate($a) {
  translate($a, "”’", "&#34;&#39;")
};

declare function local:chain-replace($a) {
  fold-left(
   for $i in $ru2
   return
    replace(?, $i?m, $i?s),
     $a,
    function($result, $curr) { $curr($result) }
           )
};

declare function local:calculate-offset($position, $marks)
 {
     let $mark   := head($marks)
     let $position := if(count($position) = 0)
      then (<t path="{$mark/@path}"
               start="1" long="{string-length($mark)}">{$mark/text()}</t>)
      else (
          $position,
          element t {    attribute path {$mark/@path},
                         attribute start {$position[count($position)]/@start
                                         + $position[count($position)]/@long},
                         attribute long {string-length($mark)}, $mark/text()}
             )
     return (
        if(count($marks) > 1) then
          local:calculate-offset($position, tail($marks))
        else $position
      )
};



declare function local:normalize-spelling($text, $file){
for $o in $text//mark
let $analyze-href :=  analyze-string(replace($o/@*:href,
                      ".xpointer\(string-range\(", "") =>
                      replace("\)\)", ""), ",")//fn:non-match
let $path := $analyze-href[1]
let $start := $analyze-href[3]
let $long := $analyze-href[4]
let $text1 := fetch:xml(replace($file, ".tok01", ""))

let $f := xquery:eval($path, map {"" : $text1})
return
 let $token :=
 (: each <token/> contains  all the t(ext) nodes relevant to each token :)
 <token form=
 "{
  substring($f, $start, $long) =>
  normalize-unicode('NFD') => replace('\p{IsCombiningDiacriticalMarks}', '')
  => local:decapitalize()
  => local:treat-j()
  => local:treat-u()
  => local:translate()
  => local:chain-replace()
  }"
 start="{$start}" long="{$long}">{
  let $b:= <textNodes>{
           local:calculate-offset((),
           for $p in $f//text()
           return
            <t path="{path($p)}">{$p}</t>)
           }</textNodes>
  for $cp in $b/t
  let $range-text-node := xs:integer($cp/@start) to
                         (xs:integer($cp/@start) + xs:integer($cp/@long))
  let $range-token := xs:integer($start)
                      to xs:integer($start) + xs:integer($long)
  where $range-token = $range-text-node
  return
  $cp}
  </token>
  return
    (: it tries to identify abbr. such as nuctu's, i.e., nuctus es :)
    let $c2 := for $g in $token/t
               where contains($g/@path, "abbr")
               let $fv := xquery:eval(
                          $token/t/@path[contains(., "abbr")], map {"" : $text1}
                                )
                                  /ancestor::*:choice/*:abbr/text()
               where $fv => contains("'")
               where not($fv => contains("'."))(: There are abbr. such as M'. :)
               return
               $g
   (: it tries to identify abbr. such as L., i.e., Lucius :)
    let $c3 := for $g in $token/t
               where contains($g/@path, "abbr")
               where xquery:eval(
                          $token/t/@path[contains(., "abbr")], map {"" : $text1}
                                )
                                  /ancestor::*:choice/*:abbr/text()
                     => contains(".")
               return
               $g


    return
    if ($c2) then
    (
    <feat xlink:href="{$o/@id}" value="{
     let $c3 := xquery:eval(
                          $token/t/@path[contains(., "abbr")], map {"" : $text1}
                          )
                           /ancestor::*:choice/*:expan/text()
    let $u := $c3 => tokenize(" ")
    let $r1 := strings:levenshtein($u[1], $token/@form)
    let $r2 := strings:levenshtein($u[2], $token/@form)
    return
    if ($r1 > $r2)
    then
    $u[1] =>
    normalize-unicode('NFD') => replace('\p{IsCombiningDiacriticalMarks}', '')
    => local:decapitalize() => local:chain-replace()
    else
    $u[2] =>
    normalize-unicode('NFD') => replace('\p{IsCombiningDiacriticalMarks}', '')
    => local:decapitalize() => local:chain-replace()
    }"/>,
    comment {$o/following::sibling:comment()[1]}
    )
    else if ($c3) then
    (
    <feat xlink:href="{$o/@id}" value="{xquery:eval(
                          $token/t/@path[contains(., "abbr")], map {"" : $text1}
                          )
                           /ancestor::*:choice/*:expan/text()}"/>,
    comment {$o/following::sibling:comment()[1]}
    )
    else
    (
    <feat xlink:href="{$o/@id}" value="{$token/@form}"/>,
    comment {$o/following::sibling:comment()[1]}
    )
};

let  $path := "/home/technician/Documents/" ||
            "dfg_revising_standardizing_expanding/paula/latin/final"
let $files := file:descendants($path)[matches(., "tok01.xml$")]
for $m in $files
let $base := tokenize($m, "/")[last()]
return
  file:write(replace($m, "tok01.xml$", "tok01_normalization01.xml"),
   <paula version="1.1">
   <header paula_id="{replace($base, "tok01.xml$", "tok01_normalization01")}"/>
   <featList xmlns:xlink="http://www.w3.org/1999/xlink" type="normalization"
     xml:base="{$base}">
     {
      for $u in fetch:xml($m)
      return
      local:normalize-spelling($u, $m)
     }
   </featList>
   </paula>,
             map {'method': 'xml', "omit-xml-declaration" :"no",
                  "standalone": "no", "doctype-system": "../../paula_feat.dtd"
                 }
            )
