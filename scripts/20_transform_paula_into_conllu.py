import sys
import os
from lxml import etree

#path = "/home/technician/Documents/latinnlp/texts/parsed-texts/" \
#       "phi0978.phi001.perseus-lat2/" \
#       "phi0978.phi001.perseus-lat2.tok01.sentence_seg01.xml"

#path2 = "/home/technician/Documents/latinnlp/texts/parsed-texts/" \
#        "phi0978.phi001.perseus-lat2/" \
#        "phi0978.phi001.perseus-lat2.tok01.xml"

# slow !
#namespace = "http://www.w3.org/1999/xlink"
#for t in tree.xpath("//mark/@b:href", namespaces={"b": namespace}):
#    a = t.replace("(", "")
#    b = a.replace(")", "")
#    for m in b.split(","):
#        n_path = "//mark[@id=" + "'" + m.replace("#", "") + "'" "]//following-sibling::comment()[1]"
#        c = tree2.xpath(n_path)
#        print(c)

def extract(tree):
    pid = tree.xpath("//@paula_id")
    base = tree.xpath("//@xml:base")
    l = []
    for m in tree.xpath("//mark"):
        f = m.xpath("./following-sibling::comment()[1]")[0]
        k = dict(m.attrib)
        k['fs'] = '-' if str(f) == '&#45;' else str(f).replace('<!--', '').replace('-->', '')
        k['paula_id'] = str(pid[0])
        k['base'] = str(base[0])
        l.append(k)
    return l

files = list(os.walk(sys.argv[1]))[0][2]
path = list(filter(lambda x: x.endswith("seg01.xml"), files))[0]
path2 = list(filter(lambda x: x.endswith("tok01.xml"), files))[0]

tree = etree.parse(os.path.join(sys.argv[1], path))
tree2 = etree.parse(os.path.join(sys.argv[1], path2))
c = extract(tree)
t = extract(tree2)

write = open(os.path.join(sys.argv[2], path2.replace(".xml", ".conllu")), "w")
for m in c:
        n = m['{http://www.w3.org/1999/xlink}href'].replace("(", "").replace(")", "")
        s = n.split(",")
        ln = len(s)
        print("# file_s = " + m['paula_id'] + ".xml", file=write)
        print("# file_t = " + m['base'], file=write)
        print("# id = " + m['id'], file=write)
        print("# sent = " + m['fs'], file=write)
        for cc, b in enumerate(s):
            v = list(filter(lambda x: x['id'] == b.replace("#", ""), t))[0]
            if int(cc) != (ln - 1):
                se = "\t".join((str(cc), v['fs'], "_", "_", "_","_","_","_", "_", v['id']))
            else:
                se = "\t".join((str(cc), v['fs'], "_", "_", "_","_","_","_", "_", v['id'] + "\n"))
            print(se, file=write)
write.close()

'''
find /home/technician/Documents/latinnlp/texts/parsed-texts/* -type d \
| parallel -j 24 python \
/home/technician/Documents/latinnlp/scripts/20_transform_paula_into_conllu.py \
{} "/home/technician/Desktop/conllu3/"
'''


#from pyspark.sql import SparkSession, Row
#spark = SparkSession \
#    .builder \
#    .appName("Python Spark SQL") \
#    .config("spark.some.config.option", "some-value") \
#    .getOrCreate()
#dc = spark.createDataFrame([Row(**i) for i in c])
#dc.show()
#tc = spark.createDataFrame([Row(**i) for i in t])
#tc.show()

#dc.createOrReplaceTempView("clauses")
#tc.createOrReplaceTempView("tokens")