(: $input is the directory path containing the files :)
declare variable $path external;
(: $write is the directory path where to write the files :)
declare variable $write external;

declare function local:to-conllu($path, $write) {

let $dir := file:list($path)
let $tok := fn:filter($dir, matches(?, "tok01.xml$"))
let $sen := fn:filter($dir, matches(?, "seg01.xml$"))
for $u in doc($path || "/" || $sen)//markList
return

file:write-text-lines(
  $write || replace($tok, "xml$", "conllu"),
  
  for $k in $u//mark
  let $h := replace($k/@*:href, "[()#]", "") => tokenize(",")
  let $count := count($h)
  return
  (
  "# file_s = " || $sen,
  "# file_t = " || $tok,
  "# id = " || $k/@id,
  "# sent = " || $k/following-sibling::comment()[2],
  for $t at $c in $h
  for $n in doc($path || "/" || $tok)//mark
  where $t = $n/@id
  let $gh := $n/following-sibling::comment()[1]
  return
  if ($c != $count)
  then
  string-join(  
  ($c, if ($gh = "&#45;") then "-" else $gh,
   "_" , "_" , "_" , "_" , "_" , "_" , "_" , $t
  ), "	")
  else
  string-join(  
  ($c, if ($gh = "&#45;") then "-" else $gh,
   "_" , "_" , "_" , "_" , "_" , "_" , "_" , $t || "&#10;"
  ), "	")
  )
)

};

local:to-conllu($path, $write)

(:
find /home/technician/Documents/latinnlp/texts/parsed-texts/* -type d \
| parallel -j 24 /home/technician/Desktop/basex/bin/basex \
-bpath={} \
-bwrite="/home/technician/Desktop/conllu2/" \
/home/technician/Documents/latinnlp/scripts/20_transform_paula_into_conllu.xq
:)


(:
let $g := db:open("parsed-texts")[position() = 1 to 2]
for $u in $g[matches(db:path(.), "seg01.xml" )]//markList
let $base := $u/@xml:base
let $conn := $g[tokenize(db:path(.), "/")[2] = $base]
return
  for $k in $u//mark
  let $h := replace($k/@*:href, "[()#]", "") => tokenize(",")
  return
  (
  "# file = " || $u/parent::*/header/@paula_id,
  "# id = " || $k/@id,
  "# sent = " || $k/following-sibling::comment()[2],
  let $u54 := $conn//mark/db:node-pre(.)
  for $t at $c in $h
  return
  string-join(  
  ($c, db:attribute("parsed-texts", $t, "id")[./ancestor::paula/header/@paula_id || ".xml" = $base]
  /parent::mark/following-sibling::comment()[1]/data(),
   "_" , "_" , "_" , "_" , "_" , "_" , "_" , "_"
  ), "	")
  )
:)