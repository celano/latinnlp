xquery version "3.1" encoding "utf-8"; (: BaseX 9.4.6 :)

(:
declare variable $path :=
"/home/technician/Desktop/conllu_ANNOTATED/priority-texts/phi1351.phi004.perseus-lat2.tok01.ANNOTATED.conllu";
:)
declare variable $path external;

declare variable $dir-to-write external;

declare function local:correct-tab($string)
{
if (matches($string, "&#9;&#9;"))
then local:correct-tab(replace($string,"&#9;&#9;","&#9;_&#9;"))
else $string
};

declare function local:convert-morph-feat($keys) {
(: $d is there for the cases _|key7=a and _ :)
let $d := replace($keys, "_\|", "") 
          => replace("_", "key1=-") => tokenize("\|")
return
string-join(
fn:fold-left($d,("-","-","-","-","-","-","-","-"),
  function($result, $curr) 
  { for $m at $c in $result
    let $t := tokenize($curr, "=")
    let $num := xs:integer($t[1] => replace("key", ""))
    let $p := $t[2] 
    return 
    if ($c = $num) then $p else $m}
)
)
};

(: $dir-to-write is a directory ending in / :)
declare function local:create-lemma($path, $dir-to-write) {
let $ff := tokenize($path, '/')[last()]
return
(
file:create-dir($dir-to-write || replace($ff, ".tok01.ANNOTATED.conllu", ""))
,
file:write(
  $dir-to-write || 
  replace($ff, ".tok01.ANNOTATED.conllu", "/") ||
  replace($ff, "tok01.ANNOTATED.conllu", "tok01_lemma01.xml"),
  
<paula version="1.1">
<header paula_id="{replace($ff, "tok01.ANNOTATED.conllu", "tok01_lemma01")}"/>
<featList xmlns:xlink="http://www.w3.org/1999/xlink" 
type="lemma" xml:base="{replace($ff, "ANNOTATED.conllu", "xml")}">{

for $m in tokenize(file:read-text($path) => local:correct-tab(), "\n\n")
return
    for $q in tokenize($m, "\n")[not(starts-with(., "#"))]
    let $n := tokenize($q, "\t")
    return
    (<feat xlink:href="{'#' || $n[10]}" value="{$n[3]}"/>,
    comment {$n[2]})
}</featList>
</paula>,
 map {'method': 'xml', "omit-xml-declaration" :"no",
      "standalone": "no", "doctype-system": "../../paula_feat.dtd"
     }
)
)

};

(: $dir-to-write is a directory ending in / :)
declare function local:create-postag($path, $dir-to-write) {
let $ff := tokenize($path, '/')[last()]
return
(
file:create-dir($dir-to-write || replace($ff, ".tok01.ANNOTATED.conllu", ""))
,

file:write(
  $dir-to-write || 
  replace($ff, ".tok01.ANNOTATED.conllu", "/") ||
  replace($ff, "tok01.ANNOTATED.conllu", "tok01_postag01.xml"),

<paula version="1.1">
<header paula_id="{replace($ff, "tok01.ANNOTATED.conllu", "tok01_postag01")}"/>
<featList xmlns:xlink="http://www.w3.org/1999/xlink" 
type="postag" xml:base="{replace($ff, "ANNOTATED.conllu", "xml")}">{
for $m in tokenize(file:read-text($path), "\n\n")
return
    for $q in tokenize($m, "\n")[not(starts-with(., "#"))]
    let $n := tokenize($q, "\t")
    return
    (<feat xlink:href="{'#' || $n[10]}" value="{$n[4] || 
                                           local:convert-morph-feat($n[6])}"/>,
    comment {$n[2]})
}</featList>
</paula>,
 map {'method': 'xml', "omit-xml-declaration" :"no",
      "standalone": "no", "doctype-system": "../../paula_feat.dtd"
     }
)     
)
};

(: this function creates the base to create the files for syntactic dependencies 
   and syntactic labels :)
declare function local:base-syntax($path) {

let $z := for $m in tokenize(file:read-text($path) => local:correct-tab(), "\n\n")
          let $s :=
           <r>{
            for $k in tokenize($m, "\n")
            where not(starts-with($k, "#"))
            return
            analyze-string($k, "\t")
           }</r>
    return
      for $q at $c in tokenize($m, "\n")[not(starts-with(., "#"))]
      let $n := tokenize($q, "\t")
      where $n[7] != "0"
      let $gov := $s/fn:analyze-string-result[xs:integer($n[7])]/fn:non-match[10]
      return
      <feat href="{'#' || $gov}" target="{'#' || $n[10]}" 
       value="{$n[8]}" head="{$gov/parent::*/fn:non-match[2]}" dep="{$n[2]}"/>
for $u in $z
count $m
return
element feat {attribute id {"dep_" || $m}, $u/@*} 
};

(: $dir-to-write is a directory ending in / :)
declare function local:create-dep($path, $dir-to-write) {
let $ff := tokenize($path, '/')[last()]
return
(
file:create-dir($dir-to-write || replace($ff, ".tok01.ANNOTATED.conllu", ""))
,

file:write(
  $dir-to-write || 
  replace($ff, ".tok01.ANNOTATED.conllu", "/") ||
  replace($ff, "tok01.ANNOTATED.conllu", "tok01_dep01.xml"),

<paula version="1.1">
<header paula_id="{replace($ff, "tok01.ANNOTATED.conllu", "tok01_dep01")}"/>
<relList xmlns:xlink="http://www.w3.org/1999/xlink" 
type="dep" xml:base="{replace($ff, "ANNOTATED.conllu", "xml")}">{

for $z in local:base-syntax($path) 
return
(element rel {attribute id {$z/@id}, attribute xlink:href {$z/@href}, 
              attribute target {$z/@target}},
 comment {$z/@head || " " || $z/@dep}
)
}</relList>
</paula>,
 map {'method': 'xml', "omit-xml-declaration" :"no",
      "standalone": "no", "doctype-system": "../../paula_rel.dtd"
     }
)
)
};

(: $dir-to-write is a directory ending in / :)
(: this function associates a syntactic label to a dep arch: however, 
   since there is no dependency for the root, the syntactic labels are attached
   to the tokens (see following function) :)
declare function local:create-dep-fnc($path, $dir-to-write) {
let $ff := tokenize($path, '/')[last()]
return
(
file:create-dir($dir-to-write || replace($ff, ".tok01.ANNOTATED.conllu", ""))
,

file:write(
  $dir-to-write || 
  replace($ff, ".tok01.ANNOTATED.conllu", "/") ||
  replace($ff, "tok01.ANNOTATED.conllu", "tok01_dep-fnc01.xml"),

<paula version="1.1">
<header paula_id="{replace($ff, "tok01.ANNOTATED.conllu", "tok01_dep-fnc01")}"/>
<relList xmlns:xlink="http://www.w3.org/1999/xlink" 
type="dep_fnc" xml:base="{replace($ff, "tok01.ANNOTATED.conllu", "tok01_dep01.xml")}">{

for $z in local:base-syntax($path) 
return
(
element feat {attribute xlink:href {'#' || $z/@id}, 
              attribute value {$z/@value}},
comment {$z/@head || " " || $z/@dep}            
)
}</relList>
</paula>,
 map {'method': 'xml', "omit-xml-declaration" :"no",
      "standalone": "no", "doctype-system": "../../paula_feat.dtd"
     }
)
)
};

(: this was created to attach labels to tokens instead of archs :)
declare function local:create-dep-fnc2($path, $dir-to-write) {
let $ff := tokenize($path, '/')[last()]
return
(
file:create-dir($dir-to-write || replace($ff, ".tok01.ANNOTATED.conllu", ""))
,
file:write(
  $dir-to-write || 
  replace($ff, ".tok01.ANNOTATED.conllu", "/") ||
  replace($ff, "tok01.ANNOTATED.conllu", "tok01_dep-fnc01.xml"),
  
<paula version="1.1">
<header paula_id="{replace($ff, "tok01.ANNOTATED.conllu", "tok01_dep-fnc01")}"/>
<featList xmlns:xlink="http://www.w3.org/1999/xlink" 
type="dep_fnc" xml:base="{replace($ff, "ANNOTATED.conllu", "xml")}">{
for $m in tokenize(file:read-text($path), "\n\n")
return
    for $q in tokenize($m, "\n")[not(starts-with(., "#"))]
    let $n := tokenize($q, "\t")
    return
    (
    <feat xlink:href="{'#' || $n[10]}" value="{$n[8]}"/>,
    comment {$n[2]}
    )
}</featList>
</paula>,
 map {'method': 'xml', "omit-xml-declaration" :"no",
      "standalone": "no", "doctype-system": "../../paula_feat.dtd"
     }
)
)
};

local:create-lemma($path, $dir-to-write), 
local:create-postag($path, $dir-to-write),
local:create-dep($path, $dir-to-write),
local:create-dep-fnc($path, $dir-to-write)


(:
find /home/technician/Documents/latinnlp/combo -name *ANNOTATED.conllu \
| parallel "/home/technician/basex946/bin/basex \
-bpath={} \
-bdir-to-write=/home/technician/Desktop/prova/ \
/home/technician/Documents/latinnlp/scripts/21_transform_annotated_conllu_into_paula.xq"
:)