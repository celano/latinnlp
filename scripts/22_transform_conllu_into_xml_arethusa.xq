xquery version "3.1" encoding "utf-8"; (: BaseX 9.4.6 :)

declare variable $path :=
"/home/technician/Documents/latinnlp/combo/texts3/phi1351.phi004.perseus-lat2.tok01.ANNOTATED.conllu";

(: dir-to-write is the directory where to write, ending with / :)
declare function local:create-xml-arethusa($path, $dir-to-write) {
file:write($dir-to-write 
|| tokenize(($path, "/")[last()] => replace("conllu", "xml") )
,

<treebank version="2.1" xml:lang="la" format="">
{
for $m at $c in tokenize(file:read-text($path), "\n\n")
return
  <sentence id="{$c}">{
    for $h in tokenize($m, "\n")
    return  
    if (starts-with($h, "# file_t"))
    then attribute document_id {$h => replace("# file_t  = ", "")}
    else if (not(starts-with($h, "#")))
    then
      let $f := (tokenize($h), "\t")
      return
      <word id="{xs:integer($f[1]) + 1}" form="{$f[2]}" lemma="{$f[3]}">
      {
       attribute pos { 
       if ($f[6] = "_") then $f[4] || "--------"
       else  
       let $j := tokenize(($f[6] => replace("_\|", "")), "\|")
       return
       $f[4] 
       ||
       string-join(
        fn:fold-left($j, ("-","-","-","-","-","-","-","-"),
          function($result, $curr) 
          { 
           let $g := tokenize($curr => replace("key", ""), "=")
           for $h at $c2 in $result 
           return 
           if ($c2 = xs:integer($g[1])) then $g[2] else $h 
          }
                    )
       , "")
      },

attribute head {$f[7]},
attribute relation {$f[8]},
attribute misc {$f[10]}
}
</word>
}</sentence>
}</treebank>

)
};

declare function local:create-xml-arethusa($path) {
<treebank version="2.1" xml:lang="la" format="">
{
for $m at $c in tokenize(file:read-text($path), "\n\n")
return
  <sentence id="{$c}">{
    for $h in tokenize($m, "\n")
    return  
    if (starts-with($h, "# file_t"))
    then attribute document_id {$h => replace("# file_t  = ", "")}
    else if (not(starts-with($h, "#")))
    then
      let $f := (tokenize($h), "\t")
      return
      <word id="{xs:integer($f[1]) + 1}" form="{$f[2]}" lemma="{$f[3]}">
      {
       attribute postag { 
       if ($f[6] = "_") then $f[4] || "--------"
       else  
       let $j := tokenize(($f[6] => replace("_\|", "")), "\|")
       return
       $f[4] 
       ||
       string-join(
        fn:fold-left($j, ("-","-","-","-","-","-","-","-"),
          function($result, $curr) 
          { 
           let $g := tokenize($curr => replace("key", ""), "=")
           for $h at $c2 in $result 
           return 
           if ($c2 = xs:integer($g[1])) then $g[2] else $h 
          }
                    )
       , "")
      },

attribute head {$f[7]},
attribute relation {$f[8]},
attribute misc {$f[10]}
}
</word>
}</sentence>
}</treebank>
};

local:create-xml-arethusa($path)