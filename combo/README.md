# Files Automatically Annotated for Morphosyntax

This directory contains all the files in `texts` annotated automatically
using the parser [COMBO](https://git.informatik.uni-leipzig.de/celano/COMBO_for_Latin/-/tree/master).