# Webanno

This folder contains webanno-related files, which are made available if anyone will annotate the Latin morphosyntax using Webanno.

The file `LatinTreebank_2019-04-26_1411.zip` contains a project to annotate
Latin in Webanno. The file has been automatically created via the export
function in Webanno. This project was created using the Webanno instantiation
(3.1.0) available at

https://www.informatik.uni-leipzig.de/webanno/

The above `zip` file contains the tagsets to annotate
morphosyntax according to the AGLDT annotation scheme. The text of Caesar's
*De Bello Civili*, which has been automatically pre-annotated using COMBO,
is also provided as an example text. The already existing annotation can be
changed using Webanno.
