At last the love I've waited for has come.

(No shame to say so: more to cover up).

My Camenae called on her in prayer,

and Cytherea brought him to my heart.

Venus kept her promise: now she can tell

my tale of joy to those who don't believe.

I hardly want to give this letter up

so no one else sees it before he does.

I'm glad I did it — why wear a prudish mask,

as if he wasn't good enough for me!

My stupid birthday's here, and I'm supposed

to go away and leave Cerinthus here.

What's better than the city? On the farm,

it's cold and rustic — no place for a girl.

Messalla, uncle, you're thinking of me,

but stop it: this is no time for a trip.

Take me away, I'll leave my heart and mind

In Rome : what good's free will? You make the rules.

You know, that trip's been taken off my mind:

your girlfriend gets to spend her day in Rome .

Let's spend the day together, as we hoped:

we've had the good luck you were waiting for.

I should be glad you think that you can cheat:

what a fool I'd be to fall into your arms.

Go: court your working girl as she spins her wool

instead of Sulpicia daughter of Servius.

My family are concerned I might lose out

to the affections of an unknown's bed.

Cerinthus, do you care that I am sick,

my body wracked with fever and fatigue?

I cannot hope that I'll be well again

unless I think that that's what you want too.

But if my sickness doesn't worry you,

what good is it to me to be well again?

My light, may you not love me any more

the way I think you did a few days since,

if I have ever in my youthful life

done anything as stupid as I did

yesterday, when I left you all alone

because I didn't want to show my love.