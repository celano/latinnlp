# Original Files

This directory contains the original 316 (PerseusDL) texts used for tokenization/sentence split (release: `canonical-latinLit-0.0.403`) [1]. The data in the present directory are, however, slightly  different from those in the original release, because a few clear typos have been corrected and, _most importantly_, the markup of the `<choice/>` element has been uniformed thus:

```
<choice>
  <corr>loquacium</corr>
  <sic>loquliciuin</sic>
<choise>
```
```
<choice>
  <abbr>consuetu's</abbr>
  <expan>consuetus es</expan>
</choice>
```
```
<choice>
  <reg>aer</reg>
  <orig>aër</orig>
</choice>
```

---
[1] https://github.com/PerseusDL/canonical-latinLit/releases/tag/0.0.403
