# Abbreviations

This directory contains a file with Latin abbreviations. They are usuful for
a number of NLP tasks, including tokenization. This list is used by Verbator [1].

---
[1] https://git.informatik.uni-leipzig.de/celano/latinnlp/-/tree/master/tokenize/tokenizer
