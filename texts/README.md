# PAULA

This directory contains the annotated files (those in `texts`) in the PAULA XML 1.1 format. The only difference from the texts in `texts` is that the "base texts" which annotation units are attached to have been extracted from the original TEI XML files.
