# Standoff Annotation for the Ancient Greek and Latin Dependency Treebank

This directory contains the code to create standoff annotation for
Caesar's ***De Bello Civili*** associated with the publication:

* Giuseppe G. A. Celano. 2019. Standoff Annotation for the Ancient Greek and 
Latin Dependency Treebank. 
In 3rd International Conference on Digital Access to Textual Cultural Heritage 
(DATeCH2019), May 8–10, 2019, Brussels, Belgium. ACM, New York, NY, USA, 149–153.
https://doi.org/10.1145/3322905.3322919

The file `BaseX.jar` is the XQuery processor used to run the code in the `scripts` directory.
The `jar` file gives access to the BaseX 9.2 GUI (the full version
of BaseX is available at [1]). 

The `scripts` directory contains the two files that generate the files in 
the `caesar-de-bello-civili/phi0448.phi002.perseus-lat2` directory, which
also contains the original XML file to tokenize 
`phi0448.phi002.perseus-lat2.xml`. More precisely:

* `01_PAULA_tokenize.xq` creates `phi0448.phi002.perseus-lat2.tok01.xml` 
* `02_PAULA_sentence_seg.xq` creates `phi0448.phi002.perseus-lat2.sent-segm01.xml`

Note that if one wants to generate the XML files again, 
the file paths in the scripts need to be changed; moreover, since the scripts rely on
external files which are over time updated, the outputs are likely to be slightly 
different.

---
[1] http://files.basex.org/releases/9.2/
