xquery version "3.1" encoding "utf-8"; (: BaseX 9.1 :)

declare namespace xlink="http://www.w3.org/1999/xlink";
declare option db:chop 'false';

declare variable $abb :=
fetch:xml(
"https://git.informatik.uni-leipzig.de/celano/latinnlp/raw/master/" ||
"abbreviations/abbr-in-texts.xml"
);

declare variable $to-tokenize :=
fetch:xml(
"https://git.informatik.uni-leipzig.de/celano/latinnlp/raw/master/" ||
"tokenize/to-tokenize.xml"
);

declare variable $not-to-tokenize :=
fetch:xml(
"https://git.informatik.uni-leipzig.de/celano/latinnlp/raw/master/" ||
"tokenize/not-to-tokenize.xml"
);

declare function local:ws-tok($a)
{
for $text in $a
/*:TEI/*:text/*:body/*:div/*:div/*:div/*:div
let $path := path($text)
return
  let $e:=  element {"div"} 
                    {attribute {"path"} {$path},
                     for $u in $text//text()
                     return
                     element {$u/parent::*/name()} {fn:analyze-string($u, "\s")}
                    }

 for $tok in $e//fn:non-match[not(ancestor::*:note)]
                             [not(ancestor::*:del)]
                             [not(ancestor::*:head)] 
 let $pos := sum($tok/(preceding::fn:match union preceding::fn:non-match)
                                                              /string-length(.))
 let $length := string-length($tok)
 return
 <mark href="{replace($e/@path, "Q\{http://www.tei-c.org/ns/1.0\}", "*:")}"
       start="{$pos + 1}" long="{$length}">{$tok/text()}</mark>
};

declare function local:avoid-abbr($a)
{
for $t in $a
let $s := $t[matches(., "^\p{L}{1}\.")]
let $b := $t[. = $abb/list/a/@f]  
return
  if ($s or $b) then $t else
    for $tok in fn:analyze-string($t, "\p{P}")/* 
    let $pos := sum($tok/preceding-sibling::*/text()/string-length(.))
    let $length := string-length($tok)
    return
    <mark href="{$t/@href}"
          start="{$t/@start + $pos}" long="{$length}">{$tok/text()}</mark>
};

declare function local:to-tok($a)
{
for $a in $a
let $p := $to-tokenize/list/w
let $b := $a[. = $p/@f]
return
  if (not($b)) then $a else
    for $h in $p[@f = $a]/t
    return
    <mark href="{$a/@href}" start="{$a/@start + $h/@s -1}" 
          long="{$h/@l}">{$h/text()}</mark>
};

declare function local:que-ve-ne($a)
{
for $j in $a
let $a := $j[. = $not-to-tokenize/list/w/@f]
let $h := matches($j, "one$|ine$")
let $d := matches($j, "ove$|ive$|ave$")
let $e := fn:analyze-string($j, "que$|ve$|ne$")
return
  if ($a) then $j else
    if (not($e/fn:match) or $h or $d) then $j else 
      for $tok in $e/* 
      let $pos := sum($tok/preceding-sibling::*/text()/string-length(.))
      let $length := string-length($tok)
      return
      <mark href="{$j/@href}"
            start="{$j/@start + $pos}" long="{$length}">{$tok/text()}</mark>  
};

declare function local:transform-mark($a)
{
  for $t at $c in $a
  return
  (<mark id="{"tok" || $c}" xlink:href=
  "{'#xpointer(string-range(' || $t/@href || ',&apos;&apos;,' ||
    $t/@start || ',' || $t/@long || '))'}" />,
    
    comment {data($t)}
  )
};

declare function local:create-paula-mark($path_file, $name_file, $name_base)
{

<paula version="1.0">
<header paula_id="{$name_file}"/>
<markList xmlns:xlink="http://www.w3.org/1999/xlink" type="tok" 
          xml:base="{$name_base}">
{
doc($path_file) => local:ws-tok() => local:avoid-abbr() =>
local:to-tok() => local:que-ve-ne() => local:transform-mark()
}
</markList>
</paula>
};

declare function local:write-paula-mark($target_to_write, $path_file, 
                                        $name_file, $name_base)
{
file:write(
$target_to_write,
<paula version="1.0">
<header paula_id="{$name_file}"/>
<markList xmlns:xlink="http://www.w3.org/1999/xlink" type="tok" 
          xml:base="{$name_base}">
{
doc($path_file) => local:ws-tok() => local:avoid-abbr() =>
local:to-tok() => local:que-ve-ne() => local:transform-mark()
}
</markList>
</paula>,
map {'method': 'xml', "omit-xml-declaration" :"no",
      "standalone": "no", "doctype-system": "paula.dtd"
    }
)
};


local:create-paula-mark(
  
"/Users/mycomputer/Documents/dfg_revising_standardizing_expanding/" ||
"caesar_de_bello_civile/phi0448.phi002.perseus-lat2/" ||
"phi0448.phi002.perseus-lat2.xml", 

"phi0448.phi002.perseus-lat2.tok01.xml", 

"phi0448.phi002.perseus-lat2.xml")


(:
local:write-paula-mark(
"/Users/mycomputer/Documents/dfg_revising_standardizing_expanding/" ||
"caesar_de_bello_civile/phi0448.phi002.perseus-lat2/" ||
"phi0448.phi002.perseus-lat2.tok01.xml",
  
"/Users/mycomputer/Documents/dfg_revising_standardizing_expanding/" ||
"caesar_de_bello_civile/phi0448.phi002.perseus-lat2/" ||
"phi0448.phi002.perseus-lat2.xml", 

"phi0448.phi002.perseus-lat2.tok01.xml", 

"phi0448.phi002.perseus-lat2.xml")
 :)