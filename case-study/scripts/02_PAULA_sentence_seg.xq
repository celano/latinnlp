xquery version "3.1" encoding "utf-8"; (: BaseX 9.1 :)

declare namespace xlink="http://www.w3.org/1999/xlink";
declare option db:chop 'false';

declare function local:create-sentence($doc_pos)
{
for tumbling window $s in $doc_pos//mark
start  $a when fn:true() 
end  $b when $b/(following-sibling::comment()[1])/data() = 
                                                 (".", ";", ":", "?", "!")
count $r
return
(<mark id="{"clause" || $r}"
      xlink:href="{'#xpointer(id(&apos;' || ($s/@id/data())[1] ||
                  '&apos;)/range-to(id(&apos;' || ($s/@id/data())[last()] || 
                  "&apos;)))"
                  }"
/>, comment{$s/(following-sibling::comment()[1])/data()},

    comment {   
    
    string-join(
    
    for $i at $c in $s
    let $f := fn:analyze-string($i/@xlink:href, 
              "(#xpointer\(string-range\()(.*)(,'',)(.*)(,)(.*)(\)\))") 
    let $i2 := $s[position() = $c + 1]
    let $f2 := fn:analyze-string($i2/@xlink:href, 
              "(#xpointer\(string-range\()(.*)(,'',)(.*)(,)(.*)(\)\))") 
    return
    if ($i/@id = $s[last()]/@id) then ($i/following-sibling::comment())[1] else
    if ($f//fn:group[@nr="2"] = $f2//fn:group[@nr="2"]
        and 
        $f//fn:group[@nr="4"] + $f//fn:group[@nr="6"] = $f2//fn:group[@nr="4"]
       )
    then ($i/following-sibling::comment())[1]
    else ($i/following-sibling::comment())[1] || " "
     
    , "")
    
            }
    )
};    

declare function local:create-paula-mark-sent($path_file, $name_file, 
                                                                     $name_base)
{

<paula version="1.0">
<header paula_id="{$name_file}"/>
<markList xmlns:xlink="http://www.w3.org/1999/xlink" type="tok" 
          xml:base="{$name_base}">
{
doc($path_file) => local:create-sentence()
}
</markList>
</paula>
};

declare function local:write-paula-mark-sent($target_to_write, $path_file, 
                                        $name_file, $name_base)
{
file:write(
$target_to_write,
<paula version="1.0">
<header paula_id="{$name_file}"/>
<markList xmlns:xlink="http://www.w3.org/1999/xlink" type="tok" 
          xml:base="{$name_base}">
{
doc($path_file) => local:create-sentence()
}
</markList>
</paula>,
map {'method': 'xml', "omit-xml-declaration" :"no",
      "standalone": "no", "doctype-system": "paula.dtd"
    }
)
};

local:create-paula-mark-sent(  

"/Users/mycomputer/Documents/dfg_revising_standardizing_expanding/" ||
"caesar_de_bello_civile/phi0448.phi002.perseus-lat2/" || 
"phi0448.phi002.perseus-lat2.tok01.xml",

"phi0448.phi002.perseus-lat2.sent-segm01.xml"
,
"phi0448.phi002.perseus-lat2.tok01.xml"
)  
  
(:
local:write-paula-mark-sent(
"/Users/mycomputer/Documents/dfg_revising_standardizing_expanding/" || 
"caesar_de_bello_civile/phi0448.phi002.perseus-lat2/" ||
"phi0448.phi002.perseus-lat2.sent-segm01.xml",  

"/Users/mycomputer/Documents/dfg_revising_standardizing_expanding/" ||
"caesar_de_bello_civile/phi0448.phi002.perseus-lat2/" ||
"phi0448.phi002.perseus-lat2.tok.xml",

"phi0448.phi002.perseus-lat2.sent-segm01.xml"
,
"phi0448.phi002.perseus-lat2.tok01.xml"
)
:)

(:
 comment {   
    let $f := fn:analyze-string($s[1]/@xlink:href, 
              "(#xpointer\(string-range\()(.*)(,'',)(.*)(,)(.*)(\)\))")   
    let $f2 := fn:analyze-string($s[last()]/@xlink:href, 
              "(#xpointer\(string-range\()(.*)(,'',)(.*)(,)(.*)(\)\))")   
    return
    xquery:eval(   
    'declare option db:chop "false"; doc("/Users/mycomputer/Documents/' || 
    'dfg_revising_standardizing_expanding/caesar_de_bello_civile/' || 
    'phi0448.phi002.perseus-lat2/phi0448.phi002.perseus-lat2.xml")' || 
     $f//fn:group[@nr="2"])
     /substring(., $f//fn:group[@nr="4"], $f2//fn:group[@nr="4"] +
                                          $f2//fn:group[@nr="6"] -
                                          $f//fn:group[@nr="4"])  
            }


:)