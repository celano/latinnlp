# Opera Latina Adnotata

This repository contains the largest _open access_ and _scalable_ collection of Latin texts, _Opera Latina Adnotata_ (**316** files, **6,755,191** tokens, and **411,329** sentences) + related resources to analyze Latin (http://ola.informatik.uni-leipzig.de/it/index.html).   🏋️‍❤️😃

The original Latin texts have been tokenized, sentence split, morphologically, and syntactically annotated using a standoff format (Paula XML), which allows smooth expansion of the corpus **via addition of multiple annotation layers**.

The repository is organized thus (further details within each directory):
1. `texts` contains the texts annotated (look in here if you are just interested in the annotated texts)
2. `original-Latin-files` contains the original Perseus Digital Library texts used as base texts in `texts`  
3. `tokenize` contains *Verbator*, the tokenizer and sentence splitter used for the texts in `texts` (also accessible via a REST API: https://git.informatik.uni-leipzig.de/celano/latinnlp/-/tree/master/tokenize/tokenizer)
4. `abbreviations` contains a list of Latin abbreviations used for tokenization
5. `normalization` contains files useful for normalization of Latin tokens
6. `guidelines` contains documentation for the annotation of Latin (_in fieri_)
7. `scripts` contains scripts used to create the present data
8. `case-study` contains a case study documenting standoff annotation for Latin
9. `paula` contains the texts in `texts` in Paula XML 1.1 format
10. `relannis` contains the relannis version of the files in `paula` (http://pcai049.informatik.uni-leipzig.de:52480/annis3/)
11. `combo` contains all the files annotated by the COMBO parser (https://git.informatik.uni-leipzig.de/celano/COMBO_for_Latin)
12. `annotation-lists` contains some lists to document how tokens should be annotated (*in fieri*).
12. `webanno` contains files related to an annotation example in Webanno

The repository is work-in-progress (aiming to add/improve annotations)

# Contact
Dr. Giuseppe G. A. Celano<br/>
Universität Leipzig<br/>
Institute of Computer Science, NLP<br/>
Augustusplatz 10<br/>
04109 Leipzig<br/>
Deutschland<br/>
*mysurname* at informatik.uni-leipzig.de<br/>
http://asv.informatik.uni-leipzig.de/staff/Giuseppe_Celano 

DFG project website: http://agldt1.informatik.uni-leipzig.de/index.html

# Funder

<a href="http://www.dfg.de/index.jsp" target="_blank">
<img src="https://upload.wikimedia.org/wikipedia/commons/8/86/DFG-logo-blau.svg" 
width="" height="40" alt=""/>
</a>

# Licence

All the data are released under a share-alike licence, which means 
(as the licence specifies) that further data which are created/build upon the 
present data *must* also be released under the same licence. For commercial use,
ask more information.

<a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">
<img alt="Creative Commons License" style="border-width:0" 
src="https://i.creativecommons.org/l/by-nc/4.0/88x31.png" /></a><br />
This work is licensed under a <a rel="license" 
href="http://creativecommons.org/licenses/by-nc/4.0/">
Creative Commons Attribution-NonCommercial 4.0 International License</a>.
