This directory contains external documents, which I make available to complete
my documentation. In particular:

* David Bamman et al. 2007. Guidelines for the Syntactic Annotation of Latin Treebanks (v. 1.3)
(https://github.com/PerseusDL/treebank_data/blob/master/v1/latin/docs/guidelines.pdf)
* Wilhelm Brambach. 1877. Aids to Latin orthography, New York, Harper and Brothers
(https://archive.org/details/aidstolatinortho00bramrich/page/n5)