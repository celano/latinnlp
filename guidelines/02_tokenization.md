# Tokenization (for Syntactic Analysis)

The term "tokenization" is here used as a cover term for what is sometimes
referred to as _tokenization_ (whitespace-based token identification) 
and _word segmentation_ (further division of a whitespace-based token). 
In the present section, I document how the texts of 
the Perseus Treebank (Classical Latin, ca. 100 BC–400 AD) have been tokenized. 

## Introduction

Tokenization for Latin is performed by the rule-based tokenizer/sentence splitter 
Verbator [1]. 
The code for tokenization is made available [2], 
which I refer the reader to for more details [3].

<br/>
<div align="center">
<img src="https://git.informatik.uni-leipzig.de/celano/latinnlp/raw/master/guidelines/images/flowchartTokenization2.svg" 
alt="drawing" width="380"></img>
<figcaption>Fig. 1. Tokenization pipeline</figcaption>
</div>
<br/>

Fig. 1 shows the main steps of the tokenization algorithm. 
Whitespaces are most of the times enough to identify tokens, 
even though punctuation marks must be separated, unless they are part of 
abbreviations, which are identified via a word list [4] and a simple regular 
expression. Besides the clear case of enclitic "que", "ve/ue", and "ne", 
there are a few Latin words requiring specific tokenization, 
such as "mecum" or "nequid", whose list and 
tokenization rules are also identified via the use of a word list [5]. 
The algorithm then tokenizes all words ending with

* que
* ve/ue
* ne

except when such endings do not correspond to the enclitic suffixes. 
Identification of such "not-to-tokenize" "que", "ve/ue", and "ne" also occurs 
via a word list [6] and a few regular expression rules (such as ".*.cumque"). 
Orthographic variations such as "u"/"v" and "i"/"j" are mostly
handled, tentatively, within the above-mentioned word lists.

There are two main points to note when tokenizing Latin:

1. The tokenization of (very) few graphic words cannot always be predicted correctly 
out of the syntactic context. For example, words such as "quaequae" 
(= "and which" or "whoever") receive different tokenization depending 
on their meaning. This means that a rule-based algorithm, such as the one 
presented here, which, at the moment, does not take syntactic context 
into consideration, is likely to generate a few tokenization errors
requiring correction during annotation. 

2. While some tokenization rules, such as, for example, the one for 
"que" coordinating two nouns are agreed on by all treebankers, 
there are less clear-cut cases, such as "neque" or "neve", where
different tokenization rules could in principle be applied. Differences may also
depend on the stage of Latin, which may require specific rules. In this respect,
it is also to be noted that different critical editions can tokenize differently 
or even be inconsistent in themselves. The tokenization scheme here proposed and 
its accompanying documentation (see also section below) aim 
to overcome the problem of (purely) orthographic variations in Latin, which, 
as is known, unduly affect morphosyntactic annotation.

While all tokenization rules can be known by inspecting the tokenization 
algorithms used for each text + the word lists ([4], [5], and [6]) presented
above, I explain the reasons of the most noticeable cases 
in the following section (see [5], though, for a complete list of the words to 
tokenize).

## Tokenization Rules

Two principles have been followed to tokenize a graphic word:

1. A graphic word is tokenized if this is necessary in order to build a 
correct syntactic tree. A clear example is enclitic "que". This principle should be 
applied considering all possible contexts in which a word can occur. For 
example, even if "neque" could be theoretically taken as a single lemma, there
are contexts in which the negation clearly needs to be split 
from the conjunction, because their scopes are different. If this were not done,
the morphosyntactic tree would contain an error (see below for
an example).

2. If a grafic word has also a split variant, the latter is chosen. For example,
one can read both "Quamobrem" and "Quam ob rem", "quomodo" and "quo modo", 
"postquam" or "post quam", but only, for example, "postea" (not "post ea" as an
adverb). In this respect, I consulted grammars [7] and dictionaries [8] before
compiling the wordlist in [5]. There are some borderline cases, 
such as "sicut" or "tamquam", whose functions do not always 
seem to correspond to those of their split variants. I therefore leave them as
they are in the original texts.

### ante quam, antea quam
These groups are often split. The clause introduced by the 
subordinate conjunction "quam" was originally comparative. 
In Classical Latin, the adverbs can also be placed far away from "quam", which 
provides evidence that univerbation occurred later (see Old Italian "poi che
" and "poiché").

### neque, nec, neve, neue, neu

Contrary to what has been done for other Latin treebanks, I separate "que"/"c" 
in "neque"/"nec" and "ve" in "neque"/"neve"/"neu", because, otherwise, it is 
sometimes impossible to build a correct syntactic tree, as the following example 
shows:

Postquam omnes Belgarum copias in unum locum coactas ad se venire vidit neque 
iam longe abesse ab iis quos miserat exploratoribus et ab Remis cognovit, 
flumen Axonam, quod est in extremis Remorum finibus, exercitum traducere 
maturavit atque ibi castra posuit. (Caesar, De Bello Gallico 2.5.4)

<br/>
<div align="center">
<img src="https://git.informatik.uni-leipzig.de/celano/latinnlp/raw/master/guidelines/images/tree.svg" 
alt="drawing" width="800" height=""></img>
<figcaption>Fig. 2. Partial tree for Caes. De Bello Gallico 2.5.4</figcaption>
</div>
<br/>

If "neque" were not tokenized, it would be impossible to associate the negation
to "abesse" (or "longe").

### post quam, postea quam
see "ante quam"

-----
[1] Indeed, the current statistical tokenizers for Latin rely on data 
whose tokenization rules are not fully disclosed: therefore, a documented 
rule-based approach, such as the one here presented, 
allows fuller control over the output. Moreover, Latin can be efficiently tokenized rule-based 
because graphic words usually correspond to tokens, and even when a graphic word needs to be split, 
ambiguities (e.g., quaeque vs. quae que) arise rarely. Verbator proves to be much more accurate than 
UDPipe-Perseus: https://git.informatik.uni-leipzig.de/celano/latinnlp/-/blob/master/tokenize/tokenizer/test/README.md<br/>
[2] https://git.informatik.uni-leipzig.de/celano/latinnlp/tree/master/scripts <br/>
[3] The script produces the initial tokenization of a text. However, it relies 
on public word lists that may be over time refined. <br/>
[4] https://git.informatik.uni-leipzig.de/celano/latinnlp/blob/master/abbreviations/abbr-in-texts.xml <br/>
[5] https://git.informatik.uni-leipzig.de/celano/latinnlp/blob/master/tokenize/to-tokenize.xml <br/>
[6] https://git.informatik.uni-leipzig.de/celano/latinnlp/blob/master/tokenize/not-to-tokenize.xml <br/>
[7] Hoffmann, Johann Baptist, and Anton Szantyr. 1965. *Lateinische Syntax und Stilistik*.
C. H. Beck; Traina Alfonso and Tullio Bertotti. 2003. *Sintassi Normativa della Lingua Latina*. Pàtron Editore;
Ernout Alfred and François Thomas. 1953. *Syntaxe Latine*. Klincksiek.<br/>
[8] Bailey Cyril et al. 1968. *Oxford Latin Dictionary*. OUP.<br/>
