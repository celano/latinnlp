# Morphological Annotation

Morphological annotation comprises annotation of part of speech, morphological
features, and lemma.

The morphological annotation for the Perseus Treebank consists of 9 fields, 
which are encoded as a 9-character long string. If a field is not relevant,
a hyphen is found: for example, the morphology of *lupus* is encoded as
`n-s---mn-`, i.e., noun, singular, masculine, nominative. 
In the following subsections the possible values for each field are given. I
also 

## 1: Part of Speech

* `n`: noun
* `v`: verb
* `a`: adjective
* `d`: adverb
* `c`: conjunction
* `r`: preposition/adposition
* `p`: pronoun
* `m`: numeral
* `i`: interjection
* `u`: punctuation

## 2: Person

* `1`: first person
* `2`: second person
* `3`: third person
* `-`: the category does not apply

## 3: Number

* `s`: singular
* `p`: plural
* `-`: the category does not apply

## 4: Tense

* `p`: present
* `i`: imperfect
* `r`: perfect
* `l`: pluperfect
* `t`: future perfect
* `f`: future
* `-`: the category does not apply

## 5: Mood

* `i`: indicative
* `s`: subjunctive
* `n`: infinitive
* `m`: imperative
* `p`: participle
* `d`: gerund
* `g`: gerundive
* `u`: supine
* `-`: the category does not apply

## 6: Voice

* `a`: active
* `p`: passive
* `d`: deponens
* `-`: the category does not apply

## 7: Gender

* `m`: masculine
* `f`: feminine
* `n`: neuter
* `-`: the category does not apply

## 8: Case

* `n`:	nominative
* `g`:	genitive
* `d`:	dative
* `a`:	accusative
* `b`:	ablative
* `v`:	vocative
* `l`:	locative
* `-`: the category does not apply

## 9: Degree

* `p`: positive
* `c`: comparative
* `s`: superlative
* `-`: the category does not apply

## Prototypical examples of postags 

noun: *rumores*, `n-p---ma-` <br/>
subjunctive verb: *esset*, `v3sisa---` <br/>
gerund: *coniurandi*, `v---d--g-` <br/>

### Lemma

Printed Latin dictionaries show a fairly high degree of inconsistence in the
criteria used to associate a lemma to word forms. For example, it is never 
explained why some substantivized adjectives are subsumed under the 
corresponding adjective lemmas, while others are promoted to separate entries. 
A similar problem is posed by participles, which are usually subsumed under
the corresponding verb entries, but not rarely also have separate entries 
as adjectives or nouns. Such inconsistencies are not only between, but also
within, dictionaries.

In order to create a digital database which *maximizes annotation 
consistency*, pricise criteria must be set. In general, strictly 
morphological criteria are adopted.

Participles which can be reconducted to a verb are always lemmatized under the
corresponding verb (e.g., `falsus` to `fallo`), no matter its synstactic function.
Those participles which derive from verbs which are not elsewhere attested or not
recognized as separate entries by dictionaries get their own lemma 
(e.g., `incorruptam` has lemma
`incorruptus`, because `incorrumpo` is not recognized as verb in most/all dictionaries).

### Grade for Adjectives, Participles and Adverbs

As a default, they get 'positive' in the 9th position, unless they are 
comparative or superlative. (`positive` does not imply that they could
have a comparative or a superlative form).

### Gerund and Gerundive

Notably, the gerund and the gerundive are not annotated for tense and voice.
The annotated categories are:

(gerund)
adsentandi: `v-s-d--g-` (i.e., verb, singular, gerund, genitive)

(gerundive)
dicendus: `v-s-g-mn-` (i.e., verb, singular, gerundive, masculine, nominative)

### Voice of Deponent Verbs

Those forms who have passive form but active meaning get voice `deponens`. The
active forms (such as the present participle) get `active` voice.

### Pronoun

Pronoun is a category which applies to any token which substitutes for a noun,
such as `hic`. The category is also used for adjectival use (i.e., `hic consul`), but
not for adverbial use (`ita` or `ibi` are adverbs).

### Substantivized Adjective vs Nouns

In Latin adjectives normally represent a clearly distinguishable *morphological* 
class (with few exceptions, such as, for example, adjectives of one 
termination).

Virtually all adjectives can potentially be substantivized, and so used as 
nouns. For the sake of annotation, therefore, one might decide to always 
annotate adjectives as such or, on the contrary, to annotate them as nouns, 
whenever they are substantivized.

*we follow the common practice to annotate adjectives as such, even when they are
substantivized (e.g., *Romani*)*
. This is consistent with the fact that also
participles are annotated as verbs, even when they are substantivized.
The same applies to pronouns, which are always annotated morphologically as
pronouns, irrespective of whether they are used as nouns or adjectives.

### Deverbal Nouns of the 4th Declension

Deverbal nouns of the 4 declension are annotated as nouns (e.g., usus, usus)

### Personal Nouns

Personal names often consist of more than one name. Conventionally, each of them is annotated morphologically as a noun

