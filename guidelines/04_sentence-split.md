# Sentence split

Sentence division in treeebanking 
relies on the one present in a given critical edition.
However, it is to be noted that the use of punctuation marks, such as the period,
colon, and semicolon, is _not always_ consistent among different editors.

Use variation of punctuation marks in critical editions 
also depends on the original language of an editor: for example, an Italian
editor adds punctuation marks according to the rules of the Italian language,
while an English one according to those of the English language. Unfortunately,
there has been no attempt to standardize the use of punctuation marks across 
critical editions. One should also note that, within the same modern language,
there are often (very) different punctuation styles, which are used interchangeably:
for example, the position of the punctuation marks quotes and full stop can vary
(." and ".).'

Punctuation marks in critical editions are usually introduced by editors,
but some of them may correspond to some ancient punctuation marks found 
in manuscripts. A stictly formal criterion is followed here to identify 
sentences (this usually has the advantage to keep sentences short,
which helps treebanking). The following punctuation marks:

1. _period_ (.)
2. _colon_ (:)
3. _semicolon_ (;) 
4. _question mark_ (?) 
4. _exclamation mark_ (!) 

are _always_ considered sentence delimiters. Verbator [1] also tries to identify
cases of multiple final punctuation marks (such as _."_ or _"._) (see [2] or [3],
which contain the same logic). It is to be noted that in Latin 
periods are also used for abbreviations, and that the first word after 
a sentence is not always capitalized.

It often happens that there are a number of infinitive clauses
in Latin which all depend on a *verbum dicendi*. Such clauses are usually 
separated by periods, colon, or semicolons. Such punctuation marks can be taken 
to identify different sentences, where the infinitive is made dependent on 
an elliptical node (i.e., it gets a syntactic label such as, for 
example, *OBJ_EXD0_PRED*).

---
[1] https://git.informatik.uni-leipzig.de/celano/latinnlp/-/tree/master/tokenize/tokenizer <br/>
[2] https://git.informatik.uni-leipzig.de/celano/latinnlp/-/blob/master/tokenize/tokenizer/verbator.xq <br/>
[3] https://git.informatik.uni-leipzig.de/celano/latinnlp/-/blob/master/scripts/02.00_paula_sentence_seg.xq