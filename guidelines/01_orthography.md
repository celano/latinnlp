# Orthography

Latin texts (even of the same age) 
can show orthographic variants [1]. The entirity of the texts of 
the Perseus Treebank derives from digitization of critical editions [2], 
where different orthographic choices on the part of editors 
can occur (for example, 
the alternation of "u"/"v" and "i"/"j" in pairs such as "vinum"/"uinum" 
and "iam"/"jam").

The texts of critical editions have been kept unchanged: 
any kind of orthographic annotation/normalization [3] for graphic words/tokens 
should be applied _stand-off as a specific layer of annotation_.
It is important to note that there exist different spelling normalizations.
The one presented here aims to facilitate queries across all texts by 
always writing spelling variants, such as those concerning the distinctions 
between "u" and "v" or "ads-" and "ass-", the same way.
For this task, I have followed Brambach's rules [4], which have been widely followed 
in critical aditions since their appearance. 
Brambach's Latin orthography, which coincides with that of 
the Silver Age, can be considered
as a kind of *practical orthography*, whose aim is to allow interoperability 
between texts of different ages. As to differences in word 
separation, such as "nihilo minus" vs "nihilominus", 
normalization is applied/discussed at the tokenization level (see [5]).

As Brambach sometimes admits more options, I stick to the following
rules when annotating lemmas (or, more in general, 
when I write Latin, as in these guidelines):  

* the distinction between "u" and "v" is kept, but not the one between "j" and
"i". Note that both capital "u" and capital "v" is "V".
* only one "i" instead of two "ii" in words such as "eiius", including the compounds
of iacio: "abicio" instead of "abiicio"; "coicio" instead of "coiicio"
* "cervus" instead of "cervos"; "vulgus" instead of "uolgus"
* "numquam" instead of "nunquam" ("m" before guttural)
* "app" instead of "adp"
* "adn" instead of "ann"

-----
[1] For an introduction on Latin orthography, see Dennis G. Brearley, 1979, 
Texts and Studies in Latin Orthography to 1977, *The Classical World*, 72:7, 
385–392 and bibliography therein. <br/>
[2] https://github.com/PerseusDL/canonical-latinLit <br/>
[3] Notably, orthographic normalization should also include 
lowercasing (for words capitalized at the beginning of a sentence) and deletion of possible diacritrics, such as those 
showing long/short vowels. <br/>
[4] https://archive.org/details/hlfsbchleinfrla01bramgoog/page/n10 and 
https://archive.org/details/aidstolatinortho00bramrich/page/n6 <br/>
[5] https://git.informatik.uni-leipzig.de/celano/latinnlp/blob/master/guidelines/02_tokenization.md <br/>