# Annotation Guidelines for Latin

This directory contains guidelines for the tokenization, sentence split, morphology, and syntactic annotation of Latin. They contain additions and corrections to the guidelines:

* Guidelines for the Syntactic Annotation of Latin Treebanks (v. 1.3) by
David Bamman, Marco Passarotti, Gregory Crane, and Savina Raynaud (https://github.com/PerseusDL/treebank_data/blob/master/v1/latin/docs/guidelines.pdf)

The folder is work in progress.
