# Syntax

The following sections aim to complement the Latin Guidelines 
([Link](https://github.com/PerseusDL/treebank_data/blob/master/v1/latin/docs/guidelines.pdf))
showing
the annotation of those
phenomena which the latter do not cover or do not fully cover. In few cases,
we suggest an annotation style which diverge from that of the Latin Guidelines
to maximize consistency within the general annotation scheme.

### Dependents of the Verb 'sum'

An argument of the verb 'sum' gets the label `PNOM`, if it is a noun or an
adjective in the nominative (or in the same case of the subject). Otherwise,
an argument gets the `OBJ` label.

### Elliptical 'sum'

If the copula is elliptical, then their dependents get a syntactic label such as, for example, 
`PNOM_ExD0_PRED`, where `PNOM` is the label of the dependent and `PRED` is the
label of the elliptical `sum`. `ExD0` represents the elliptical node. There are
as many `ExD` as the number of elliptical nodes (`ExD0`, `ExD1`, etc).

If `sum` is used as an auxiliary (i.e., in passives such as `tactus est`),
the participle governs the auxiliary `sum` (AuxV), and therefore `sum` is not
added if missing (no elliptical node is added if it has no dependents).

### Passive and active perifrastic constructions

providendum -> est (AuxV)
iturus -> est (AuxV)

### `post urbem conditam` or `ad Neronem destituendum` Constructions

These constructions are *conventionally* annotated as

post (AuxP) -> urbem (ADV) -> conditam (ATR)

(The participle has an ambivalent nature of adjective and verb)

### Personal Names

When a name consists of more than one token (e.g., Servius Galba), 
the first one is considered the governor of the followings ones,
which all take the `ATR` label.

### AuxY

This label is used for adverbs such as `nam` and `itaque`, whose scope is a
whole sentence.

### Agent or Efficient Cuase

A token being an agent or effieicnt cause of a passive verb gets the lable `OBJ`.

### Comparative clauses

Comparative clause are very often elliptical of the verb. In order to keep
addition of elliptical nodes to a minimum, they are added only if the comparative
clause has more than one constituent (and therefore they need to be made dependent of
the verb). If there is only a constituent (typically the subject), it is made
dependent of the conjunction.

### APOS

APOS is used for appositions as traditionally defined, i.e., any noun or noun-like
constituent which refers to another noun or noun-like constituent.

### ita/adeo ut(i)

ita/adeo (ADV) -> uti (AuxC) -> demonstravimus (APOS)

### antequam

ante (AuxP) -> quam (AuxC)

